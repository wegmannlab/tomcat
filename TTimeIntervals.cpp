/*
 * TTimeIntervals.cpp
 *
 *  Created on: Dec 15, 2018
 *      Author: phaentu
 */


#include "TTimeIntervals.h"

TTimeIntervals::TTimeIntervals(){
	numActivityIntervals_nh = 0;
	numObservationIntervals_n = 0;
	activityLength = 0.0;
	observationlength = 0.0;
	numObservationPerActivityInterval = 0;
	numSecondsPerDay = 24 * 60 * 60;
	referenceDate = 0;

	K = NULL;
	K_scaled = NULL;
	observationToActivityMap = NULL;
	shift = 0;
	shift_old = 0;
	numShiftUpdates = 0;
	numShiftUpdatesAccepted = 0;

	curK = 0;
	initialized = false;
	logPriorProposalRatio = 0.0;
};

TTimeIntervals::TTimeIntervals(int NumActivityIntervals, int NumObsPerInterval, int Shift){
	numSecondsPerDay = 24 * 60 * 60;
	initialize(NumActivityIntervals, NumObsPerInterval, Shift);
};

TTimeIntervals::TTimeIntervals(TParameters & params, TLog* logfile){
	logfile->startIndent("Initializing intervals:");
	numSecondsPerDay = 24 * 60 * 60;
	int numActivityIntervals = params.getParameterIntWithDefault("numActivityIntervals", 24); //3 hour intervals

	//make sure #seconds / day is a multiple of #activity intervals, else adjust
	if(numSecondsPerDay % numActivityIntervals != 0){
		std::string report = "Adjusting numActivityIntervals from " + toString(numActivityIntervals) + " to ";

		//increase until it is a multiple
		while(numSecondsPerDay % numActivityIntervals != 0)
			++numActivityIntervals;
		logfile->list(report + toString(numActivityIntervals) + " so that the total number of seconds / day (86400) is a multiple of it.");
	}

	int numObsPerInterval = params.getParameterIntWithDefault("numObsPerInterval", 4); //15 minute intervals, 4 per 1 hour interval
	if(numSecondsPerDay % (numActivityIntervals*numObsPerInterval) != 0){
		std::string report = "Adjusting numActivityIntervals from " + toString(numObsPerInterval) + " to ";

		//increase until it is a multiple
		while(numSecondsPerDay % (numActivityIntervals*numObsPerInterval) != 0)
			++numObsPerInterval;
		logfile->list(report + toString(numObsPerInterval) + " so that the total number of seconds / day (86400) is a multiple of the total number of observation intervals per day.");
	}

	int Shift = params.getParameterIntWithDefault("shift", 0);
	if(Shift < 0 || Shift >= numObsPerInterval)
		throw "Shift " + toString(Shift) + " outside interval[0, " + toString(numObsPerInterval) + "]!";

	//initialize
	initialize(numActivityIntervals, numObsPerInterval, Shift);

	//read K
	if(params.parameterExists("K")){
		std::vector<double> K_vec(numActivityIntervals_nh);
		params.fillParameterIntoVector("K", K_vec, ',');
		setK(K_vec);
	}

	//report
	logfile->list("Will use " + toString(numActivityIntervals_nh) + " activity intervals of " + toString(activityLength / 60) + " min each.");
	logfile->list("Will use " + toString(numObservationIntervals_n) + " observation intervals (" + toString(numObservationPerActivityInterval) + " per activity interval) of " + toString(observationlength / 60) + " min each.");
	logfile->list("Will shift the observation intervals by " + toString(shift) + ".");
	std::string K_string = toString(K[0].value);
	for(int k=1; k<numActivityIntervals_nh; ++k)
		K_string += ", " + toString(K[k].value);
	logfile->list("Set K = " + K_string + ".");

	logfile->endIndent();
};

TTimeIntervals::TTimeIntervals(std::vector<std::string> & mcmcFileHeader){
	initialize(mcmcFileHeader);
};

TTimeIntervals::~TTimeIntervals(){
	if(initialized){
		delete[] K;
		delete[] K_scaled;
		delete[] observationToActivityMap;
	}
};

void TTimeIntervals::initialize(int NumActivityIntervals, int NumObsPerInterval, int Shift){
	numActivityIntervals_nh = NumActivityIntervals;
	if(86400 % numActivityIntervals_nh != 0)
		throw "Number of seconds per day is not a multiple of number of activity intervals!";

	numObservationPerActivityInterval = NumObsPerInterval;
	numObservationIntervals_n = NumObsPerInterval * NumActivityIntervals;
	if(86400 % numObservationIntervals_n != 0)
		throw "Number of seconds per day is not a multiple of number of observation intervals!";

	//calculate length in seconds
	activityLength = 86400 / numActivityIntervals_nh;
	observationlength = 86400 / numObservationIntervals_n;

	//initialize K
	K = new TMCMCParameterPositive[numActivityIntervals_nh];
	K_scaled = new double[numActivityIntervals_nh];
	setKUniform();

	//initialize observation to activity map
	observationToActivityMap = new short[numObservationIntervals_n];
	fillObservationToActivityMap(Shift);

	initialized = true;
	curK = 0;
	logPriorProposalRatio = 0.0;
};

void TTimeIntervals::initialize(std::vector<std::string> & mcmcFileHeader){
	numSecondsPerDay = 24 * 60 * 60;

	//parse header: LL, shift_in_XX_seconds, K1, K2, ..., Kn
	int NumActivityIntervals = mcmcFileHeader.size() - 2;

	std::vector<std::string> vec;
	fillVectorFromString(mcmcFileHeader[1], vec, '_');

	int obsLength = stringToInt(vec[2]);
	int NumObsPerInterval = numSecondsPerDay / NumActivityIntervals / obsLength;

	initialize(NumActivityIntervals, NumObsPerInterval, stringToInt(vec[0]));
};

void TTimeIntervals::setReferenceDate(time_t time){
	struct tm* timeinfo = gmtime(&time);
	timeinfo->tm_hour = 0;
	timeinfo->tm_min = 0;
	timeinfo->tm_sec = 0;

	referenceDate = timegm(timeinfo);
};

void TTimeIntervals::setK(std::vector<double> K_vec){
	if(K_vec.size() != (size_t) numActivityIntervals_nh)
		throw "Wrong number of K values provided: expect " + toString(numActivityIntervals_nh) + " but got " + toString(K_vec.size()) + "!";

	for(int k=0; k<numActivityIntervals_nh; ++k)
		K[k] = K_vec[k];

	normalizeK();
	fillKScaled();
};

void TTimeIntervals::normalizeK(){
	double sum = 0.0;
	for(int k=0; k<numActivityIntervals_nh; ++k)
		sum += K[k].value;

	//normalize such that sum = numActivityIntervals_nh
	//this implies that K = 1.0 is lambda_bar
	double scale = (double) numActivityIntervals_nh / sum;
	for(int k=0; k<numActivityIntervals_nh; ++k)
		K[k].scale(scale);
};

void TTimeIntervals::setKUniform(){
	for(int k=0; k<numActivityIntervals_nh; ++k)
		K[k] = 1.0;
	fillKScaled();
};


void TTimeIntervals::guessKFromObservations(uint32_t ** dataPerActivityInterval){
    // count number of intervals with data
    int numIntervalsWithData = 0;
    double sum = 0.;
    std::vector<int> totalNumberObservedIntervals(numActivityIntervals_nh);
    for (int interval = 0; interval < numActivityIntervals_nh; interval++) {
        totalNumberObservedIntervals[interval] = dataPerActivityInterval[0][interval] + dataPerActivityInterval[1][interval];

        if (totalNumberObservedIntervals[interval] > 0) {
            numIntervalsWithData++;
            sum += totalNumberObservedIntervals[interval];
        }
    }

    // average number of observation intervals -> how long all camera traps were running over all intervals on average
    std::vector<double> weightedNumberOf1s(numActivityIntervals_nh, 0.);
    double average = sum / numIntervalsWithData;

    double averageWeighted1s = 0.;
    for (int interval = 0; interval < numActivityIntervals_nh; interval++) {
        if (totalNumberObservedIntervals[interval] > 0) {
            weightedNumberOf1s[interval] = (dataPerActivityInterval[1][interval] + 1) * average /
                    (totalNumberObservedIntervals[interval] + 1);
            averageWeighted1s += weightedNumberOf1s[interval];
        }
    }
    averageWeighted1s /= numIntervalsWithData;

    // weight observations with how long camera traps were running
    for (int interval = 0; interval < numActivityIntervals_nh; interval++){
        if (totalNumberObservedIntervals[interval] > 0) {
            K[interval].value = weightedNumberOf1s[interval] / averageWeighted1s;
        } else {
            K[interval].value = 1.;
        }
    }

    fillKScaled();
};

void TTimeIntervals::fillKScaled(){
	for(int i=0; i<numActivityIntervals_nh; ++i)
		K_scaled[i] = K[i].value * observationlength;
};

void TTimeIntervals::fillObservationToActivityMap(int Shift){
	shift = Shift % numObservationPerActivityInterval;
	for(int i=0; i<numObservationIntervals_n; ++i){
		int index = i - shift;
		if(index < 0) index = numObservationIntervals_n + index;
		observationToActivityMap[i] = (index / numObservationPerActivityInterval) % numActivityIntervals_nh;
	}
};

void TTimeIntervals::setFromMcmcLine(std::vector<double> & mcmcLine){
	//first column is LL, second shift
	shift = mcmcLine[1];

	//rest are K
	for(int k=0; k<numActivityIntervals_nh; ++k)
		K[k] = mcmcLine[k+2];

	normalizeK();
	fillKScaled();
};

double TTimeIntervals::calcLogLikelihood(const double & lambdaBar, const double & p, uint32_t** data){
	double LL = 0;

	// NR: Pre-calculate data[0] and data[1] pointers to avoid costly re-computation at every loop iteration
    uint32_t* data_0 = data[0];
    uint32_t* data_1 = data[1];
	for(int i=0; i<numActivityIntervals_nh; ++i){
		//calculate lambda for activity interval
		double lambda = lambdaBar * p * K_scaled[i];

		//count ones and zeros across all observation windows
		int sum_one = 0;
		int sum_zero = 0;
		// NR: Pre-compute invariant part of index, which doesn't change in the inner loop
		int index_inv = i * numObservationPerActivityInterval + shift;
		for(int j=0; j<numObservationPerActivityInterval; ++j){
			/* NR: Only use modulo if necessary
			 * Code below same as:
			 * int index;
			 * if (index_inv + j >= numObservationIntervals_n) {
			 *     index = (index_inv + j) % numObservationIntervals_n
			 * } else {
			 *     index = (index_inv + j);
			 * }
			 */
			int index = (index_inv + j) >= numObservationIntervals_n ? (index_inv + j) % numObservationIntervals_n : (index_inv + j);
			sum_one += data_1[index];
			sum_zero += data_0[index];
		}

		//P(Y=0|theta) = exp(-lambda) -> log P(Y=1|theta) = -lambda;
		//P(Y=1|theta) = 1.0 - P(Y=0|theta) -> log P(Y=1|theta) = log(1.0 - exp(-lambda))
		//TODO! Create lookup table to interpolate?
		if(lambda > 1E-15)
			LL += log(1.0 - exp(-lambda)) * sum_one - lambda * sum_zero;
		else
			LL += -34.53958 * sum_one - lambda * sum_zero; //put minimum to avoid underflow.
	}

	return LL;
};

double TTimeIntervals::getK(const double timeOfDayFraction){
	int seconds = timeOfDayFraction * numSecondsPerDay - shift;
	if(seconds < 0)
		seconds = numSecondsPerDay + seconds;

	int index = (double) seconds / (double) numSecondsPerDay * numActivityIntervals_nh;
	return K[index].value;
};


int TTimeIntervals::getObservationInterval(time_t time){
	return ((time - referenceDate) % numSecondsPerDay ) / observationlength;
};

int TTimeIntervals::getNextObservationInterval(time_t time){
	int secOfDay = (time - referenceDate) % numSecondsPerDay;
	if(secOfDay % observationlength == 0)
		return secOfDay / observationlength;
	else
		return secOfDay / observationlength + 1;
};

time_t TTimeIntervals::getStartOfObservationInterval(time_t day, int observationInterval){
	int numDays = (day - referenceDate) / numSecondsPerDay;
	return referenceDate + numDays * numSecondsPerDay + observationInterval * observationlength;
};

std::string TTimeIntervals::getKString(){
	std::string ret = toString(K[0].value);
	for(int k=1; k<numActivityIntervals_nh; ++k)
		ret += ", " + toString(K[k].value);
	return ret;
};

bool TTimeIntervals::updateNextK(TRandomGenerator* randomGenerator){
	//restart?
	if(curK == numActivityIntervals_nh)
		curK = 0;
	else {
		++curK;
		if(curK == numActivityIntervals_nh)
			return false;
	}

	//update curK
	logPriorProposalRatio = K[curK].update(randomGenerator);
	if(K[curK].value > numActivityIntervals_nh){
		//reject update
		K[curK].reject();
		return updateNextK(randomGenerator);
	};

	//get sum of K
	double sum = 0.0;
	for(int k=0; k<numActivityIntervals_nh; ++k)
		sum += K[k].value;

	//normalize all expect curK such that sum = numActivityIntervals_nh
	//this implies that K = 1.0 is lambda_bar
	double scale = ((double) numActivityIntervals_nh - K[curK].value) / (sum - K[curK].value);
	for(int k=0; k<numActivityIntervals_nh; ++k){
		if(k != curK) K[k].scale(scale);
	}

	//fill scaled
	fillKScaled();

	return true;
};

void TTimeIntervals::rejectLastUpdate_K(){
	for(int k=0; k<numActivityIntervals_nh; ++k){
		if(k == curK) K[k].reject();
		else K[k].reset();
	}

	fillKScaled();
};

void TTimeIntervals::updateShift(TRandomGenerator* randomGenerator){
	shift_old = shift;

	if(randomGenerator->getRand() < 0.5)
		shift = (shift + 1) % numObservationPerActivityInterval;
	else {
		shift -= 1;
		if(shift < 0)
			shift = numObservationPerActivityInterval - shift;
	}
	logPriorProposalRatio = 0.0;
	++numShiftUpdates;
	++numShiftUpdatesAccepted;
};

void TTimeIntervals::rejectShitUpdate(){
	shift = shift_old;
	--numShiftUpdatesAccepted;
};

void TTimeIntervals::adjustProposalRanges(){
	for(int k=0; k<numActivityIntervals_nh; ++k)
		K[k].adjustProposal();

	numShiftUpdates = 0;
	numShiftUpdatesAccepted = 0;
};

void TTimeIntervals::writeMCMCHeader(gz::ogzstream & out){
	out << "LL\tshift_in_" << observationlength << "_seconds";
	for(int k=0; k<numActivityIntervals_nh; ++k)
		out << "\tK_" << k+1;
	out << "\n";
};

void TTimeIntervals::writeMCMCValues(gz::ogzstream & out, const double LL){
	out << LL << "\t" << shift;
	for(int k=0; k<numActivityIntervals_nh; ++k)
		out << "\t" << K[k].value;
	out << "\n";
};

void TTimeIntervals::printAcceptanceRates(TLog* logfile){
	std::string s = toString(K[0].getAcceptanceRate());
	for(int k=1; k<numActivityIntervals_nh; ++k)
		s += ", " + toString(K[k].getAcceptanceRate());
	logfile->list("K = " + s);
	logfile->list("shift = " + toString((double) numShiftUpdatesAccepted / (double) numShiftUpdates));
};
