/*
 * atlasTaskSwitcher.h
 *
 *  Created on: Dec 17, 2017
 *      Author: phaentu
 */

#ifndef TASKSWITCHER_H_
#define TASKSWITCHER_H_

#include "TDeltaTEstimator.h"
#include "TLog.h"
#include "TParameters.h"
#include "TSimulator.h"
#include "TEstimator.h"
#include "TPredictor.h"

//---------------------------------------------------------------------------
//Switch task
//---------------------------------------------------------------------------
class TaskSwitcher{
private:
	 TParameters* parameters;
	 TLog* logfile;
	 TRandomGenerator* randomGenerator;

public:
	 TaskSwitcher(TParameters* Parameters, TLog* Logfile){
		parameters = Parameters;
		logfile = Logfile;

		//initialize random generator
		logfile->listFlush("Initializing random generator ...");
		if(parameters->parameterExists("fixedSeed")){
			randomGenerator=new TRandomGenerator(parameters->getParameterLong("fixedSeed"), true);
		} else if(parameters->parameterExists("addToSeed")){
			randomGenerator=new TRandomGenerator(parameters->getParameterLong("addToSeed"), false);
		} else randomGenerator=new TRandomGenerator();
		logfile->write(" done with seed " + toString(randomGenerator->usedSeed) + "!");
	};

	 ~TaskSwitcher(){
		delete randomGenerator;
	 };

	void runTask(std::string task){
		//first all task that do not require TGenome
		if(task == "simulate"){
			logfile->startIndent("Generating simulations (task = simulate):");
			TSimulator simulator(logfile);
			simulator.runSimulations(*parameters, randomGenerator);
		} else if(task == "estimate"){
			logfile->startIndent("Estimating occupancy parameters (task = estimate):");
			TEstimator estimator(logfile, randomGenerator);
			estimator.runEstimation(*parameters);
		} else if(task == "predict"){
			logfile->startIndent("Projecting species densities (task = project):");
			TPredictor predictor(logfile);
			predictor.predict(*parameters);
		} else if(task == "deltaT"){
			logfile->startIndent("Estimating deltaT (time) between two species (task = deltaT):");
			TDeltaTEstimator deltaEstimator(logfile);
			deltaEstimator.estimateDeltaT(*parameters);
		} else if(task == "deltaS"){
			logfile->startIndent("Estimating deltaS (space) between two species (task = deltaS):");
			TDeltaSEstimator deltaEstimator(logfile);
			deltaEstimator.estimateDeltaSpace(*parameters);
		} else if(task == "deltaST"){
			logfile->startIndent("Estimating deltaST (space & time) between two species (task = deltaST):");
			TDeltaSEstimator deltaEstimator(logfile);
			deltaEstimator.estimateDeltaSpaceTime(*parameters);
		}  else if(task == "posteriorFromFile"){
            logfile->startIndent("Estimating the posterior of MCMC parameter values provided in a file (task = posteriorFromFile):");
            TEstimator estimator(logfile, randomGenerator);
            estimator.calculatePosteriorFromFile(*parameters);
		} else if(task == "standardizeEnv") {
            logfile->startIndent("Standardizing the environmental variables and writing mean and variance to file (task = standardizeEnv):");
            TEstimator estimator(logfile, randomGenerator);
            estimator.readTrapsAndStandardizeEnv(*parameters);
        } else throw "Unknown task '" + task + "'!";
		logfile->endIndent();
	};

	void runTask(){
		std::string task = parameters->getParameterString("task");
		runTask(task);
	};
};


#endif /* TASKSWITCHER_H_ */
