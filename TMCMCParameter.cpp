/*
 * TMCMCParameter.cpp
 *
 *  Created on: Dec 19, 2018
 *      Author: phaentu
 */

#include "TMCMCParameter.h"


//-------------------------------------------
// TPrior
//-------------------------------------------
TPriorUniform::TPriorUniform(){
	min = 0.0;
	max = 1.0;
	logDensity = 0.0;
};

TPriorUniform::TPriorUniform(double Min, double Max){
	initialize(Min, Max);
};

void TPriorUniform::initialize(double Min, double Max){
	min = Min;
	max = Max;
	if(min >= max) throw "Can not initialize uniform prior with min >= max!";
	logDensity = log(1.0 / (max - min));
};

double TPriorUniform::getLogPriorDensity(double x){
	if(x < min || x > max) return -std::numeric_limits<double>::infinity();
	return logDensity;
};

double TPriorUniform::getRandomValue(TRandomGenerator* randomGenerator){
	return randomGenerator->getRand(min, max);
};

//----------------------------------------------
TPriorNormal::TPriorNormal(){
    sqrtTwoPi = sqrt(2.0 * 3.1415928);
    initialize(0.0, 1.0);
};

TPriorNormal::TPriorNormal(double Mean, double Sd){
    sqrtTwoPi = sqrt(2.0 * 3.1415928);
    initialize(Mean, Sd);
};

void TPriorNormal::initialize(double Mean, double Sd){
	mean = Mean;
	sd = Sd;

	twoSdSquared = 2.0 * sd * sd;
	logOneOverSqrtTwoPiSigma = -log(sqrtTwoPi * sd);
};

double TPriorNormal::getLogPriorDensity(double x){
	double tmp = (x-mean);
	return logOneOverSqrtTwoPiSigma - tmp * tmp / twoSdSquared;
};

double TPriorNormal::getRandomValue(TRandomGenerator* randomGenerator){
	return randomGenerator->getNormalRandom(mean, sd);
};


//----------------------------------------------
TPriorExponential::TPriorExponential(){
	initialize(1.0);
};

TPriorExponential::TPriorExponential(double Lambda){
	initialize(Lambda);
};

void TPriorExponential::initialize(double Lambda){
	lambda = Lambda;
	logLambda = log(lambda);
};

double TPriorExponential::getLogPriorDensity(double x){
	return logLambda - lambda * x;
};

double TPriorExponential::getRandomValue(TRandomGenerator* randomGenerator){
	return randomGenerator->getExponentialRandom(lambda);
};


//-------------------------------------------
// TMCMCParameter
//-------------------------------------------
TMCMCParameter::TMCMCParameter(){
	value = 0.0;
	value_old = 0.0;
	updates = 0;
	acceptedUpdates = 0;
	propSD = 0.1;
	prior = NULL;
	hasPrior = false;
};

TMCMCParameter::TMCMCParameter(double val){
	value = val;
	value_old = val;
	updates = 0;
	acceptedUpdates = 0;
	propSD = 0.1;
	prior = NULL;
	hasPrior = false;
};

double TMCMCParameter::update(TRandomGenerator* & randomGenerator){
	value_old = value;
	value = randomGenerator->getNormalRandom(value, propSD);
	++acceptedUpdates;
	++updates;

	//return log(proposal ratio) + log(prior ratio)
	if(hasPrior){
		return prior->getLogPriorDensity(value) - prior->getLogPriorDensity(value_old);
	} else {
		return 0.0;
	}
};

void TMCMCParameter::reject(){
	value = value_old;
	--acceptedUpdates;
};

void TMCMCParameter::adjustProposal(){
	if(updates > 0){
		//get scaling
		double acc = getAcceptanceRate();
		double scale = 3.0 * acc;
		if(scale < 0.1) scale = 0.1;

		//update
		propSD *= scale;
		updates = 0;
		acceptedUpdates = 0;
	}
};

double TMCMCParameter::getAcceptanceRate(){
	return ((double) acceptedUpdates + 1.0) / ((double) updates + 1.0);
};

//-------------------------------------------
// TMCMCParameterTrackExp
//-------------------------------------------

TMCMCParameterTrackExp::TMCMCParameterTrackExp() : TMCMCParameter(){
    expValue = 0.0;
    expValue_old = 0.0;
};

TMCMCParameterTrackExp::TMCMCParameterTrackExp(double val) : TMCMCParameter(val){
    expValue = exp(value);
    expValue_old = exp(value_old);
};

double TMCMCParameterTrackExp::update(TRandomGenerator* & randomGenerator){
    value_old = value;
    expValue_old = expValue;

    value = randomGenerator->getNormalRandom(value, propSD);
    expValue = exp(value);

    ++acceptedUpdates;
    ++updates;

    //return log(proposal ratio) + log(prior ratio)
    if(hasPrior){
        return prior->getLogPriorDensity(expValue) - prior->getLogPriorDensity(expValue_old);
    } else {
        return 0.0;
    }
};

void TMCMCParameterTrackExp::set(double val) {
    TMCMCParameter::set(val);
    expValue = exp(value);
    expValue_old = exp(value_old);
}

void TMCMCParameterTrackExp::scale(double Scale) {
    TMCMCParameter::scale(Scale);
    expValue = exp(value);
    expValue_old = exp(value_old);
}

void TMCMCParameterTrackExp::reject(){
    TMCMCParameter::reject();
    expValue = expValue_old;
};

void TMCMCParameterTrackExp::reset() {
    TMCMCParameter::reset();
    expValue = expValue_old;
}

//-------------------------------------------
// TMCMCParameterPositive
//-------------------------------------------
double TMCMCParameterPositive::update(TRandomGenerator* & randomGenerator){
	TMCMCParameter::update(randomGenerator);

	//now mirror
	if(value < 0.0) value = fabs(value);

	//return log(proposal ratio) + log(prior ratio)
	//Here, log(proposal ratio) is 0.0 as this update is symmetric
	if(hasPrior)
		return prior->getLogPriorDensity(value) - prior->getLogPriorDensity(value_old);
	else return 0.0;
};

//-------------------------------------------
// TMCMCParameterZeroOne
//-------------------------------------------
double TMCMCParameterZeroOne::update(TRandomGenerator* & randomGenerator){
	TMCMCParameter::update(randomGenerator);

	//now mirror
	if(value < 0.0) value = fabs(value);
	else if(value > 1.0) value = 2.0 - value;

    //return log(proposal ratio) + log(prior ratio)
    //Here, log(proposal ratio) is 0.0 as this update is symmetric
    if(hasPrior)
        return prior->getLogPriorDensity(value) - prior->getLogPriorDensity(value_old);
    else return 0.0; //log(proposal ratio) is 0.0 as this update is symmetric
};

void TMCMCParameterZeroOne::adjustProposal(){
	TMCMCParameter::adjustProposal();
	//make sure it is not too large
	if(propSD > 0.25) propSD = 0.25;
};

//-------------------------------------------
// TMCMCParameterMixture
//-------------------------------------------
TMCMCParameterMixture::TMCMCParameterMixture():TMCMCParameter(){
	_init();
};

TMCMCParameterMixture::TMCMCParameterMixture(double val):TMCMCParameter(val){
	_init();
};

void TMCMCParameterMixture::addParameters(TMCMCParameter* Pi){
	_pi = Pi;
};

void TMCMCParameterMixture::addPrior(TPrior *Prior0, TPrior *Prior1) {
    _model_0 = Prior0;
    _model_1 = Prior1;
    hasPrior = true;
}

void TMCMCParameterMixture::_init(){
    model = 0;
	_counter_1_Model = 0;
	_counter_1_Model_Positive = 0;
	_counter_1_Model_Negative = 0;
	_pi = nullptr;
	_model_0 = nullptr;
	_model_1 = nullptr;
	_propSD_1 = 0.1;
	_acceptedUpdates_1 = 0;
	_updates_1 = 0;
}

void TMCMCParameterMixture::set(double val, uint8_t whichModel){
    if (whichModel == 0)
        model = 0;
    else if (whichModel == 1)
        model = 1;
    else throw std::runtime_error("In function 'void TMCMCParameterMixture::set(double val, uint8_t whichModel)': Can only choose between models 0 and 1 (not " + toString(whichModel) + ")!");
    value_old = value;
    value = val;
};

void TMCMCParameterMixture::_updateModel(TRandomGenerator* & randomGenerator){
    // calculate P(A | model)
    double probA_model0 = 0.;
    double probA_model1 = 0.;
    if (hasPrior) {
        probA_model0 = _model_0->getLogPriorDensity(value);
        probA_model1 = _model_1->getLogPriorDensity(value);
    }

    // calculate P(model | pi)
    double oddModel1_pi = log(_pi->value / (1 - _pi->value));

    // calculate hastings ratio
    double logH;
    if (model == 0){ // jump 0 -> 1
        logH = probA_model1 + oddModel1_pi - probA_model0;
    } else { // jump 1 -> 0
        logH = probA_model0 - oddModel1_pi - probA_model1;
    }

    // accept/reject
    if(log(randomGenerator->getRand()) < logH){
        // accept
        model = 1 - model;
    }

    // update counters
    if (model == 1){
        _counter_1_Model++;

        if (value >= 0)
            _counter_1_Model_Positive++;
        else
            _counter_1_Model_Negative++;
    }
}

double TMCMCParameterMixture::update(TRandomGenerator* & randomGenerator){
	// update model (0 or 1 model)
	_updateModel(randomGenerator);

    //update regularly
    value_old = value;
    if (model == 0) {
        value = randomGenerator->getNormalRandom(value, propSD);
        ++updates;
        ++acceptedUpdates;
    }
    else {
        value = randomGenerator->getNormalRandom(value, _propSD_1);
        ++_updates_1;
        ++_acceptedUpdates_1;
    }

    //return log(proposal ratio) + log(prior ratio)
    //Here, log(proposal ratio) is 0.0 as this update is symmetric
    if (model == 0)
        return _model_0->getLogPriorDensity(value) - _model_0->getLogPriorDensity(value_old);
    else
        return _model_1->getLogPriorDensity(value) - _model_1->getLogPriorDensity(value_old);
};

void TMCMCParameterMixture::adjustProposal(double newSd0, double newSd1) {
    // set new proposal kernels
    propSD = newSd0;
    _propSD_1 = newSd1;

    // reset counters
    updates = 0;
    acceptedUpdates = 0;
    _updates_1 = 0;
    _acceptedUpdates_1 = 0;

    // clear counter of model 1 after burnin
    _counter_1_Model = 0;
    _counter_1_Model_Positive = 0;
    _counter_1_Model_Negative = 0;
}

double TMCMCParameterMixture::calculateInclusionProbability(){
    return static_cast<double>(_counter_1_Model) / static_cast<double>(updates + _updates_1);
}

double TMCMCParameterMixture::calculateInclusionProbabilityPositive(){
    return static_cast<double>(_counter_1_Model_Positive) / static_cast<double>(updates + _updates_1);
}

double TMCMCParameterMixture::calculateInclusionProbabilityNegative(){
    return static_cast<double>(_counter_1_Model_Negative) / static_cast<double>(updates + _updates_1);
}

void TMCMCParameterMixture::reject(){
    value = value_old;
    if (model == 0)
        --acceptedUpdates;
    else
        --_acceptedUpdates_1;
};

double TMCMCParameterMixture::getAcceptanceRate_1() const{
    return ((double) _acceptedUpdates_1 + 1.0) / ((double) _updates_1 + 1.0);
};