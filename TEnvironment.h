/*
 * TEnvironment.h
 *
 *  Created on: Dec 16, 2018
 *      Author: phaentu
 */

#ifndef TENVIRONMENT_H_
#define TENVIRONMENT_H_

#include "stringFunctions.h"
#include "TRandomGenerator.h"
#include "gzstream.h"
#include "TLog.h"
#include "TMCMCParameter.h"
#include "TMeanVar.h"

//-------------------------------------------
// TEnvironment
//-------------------------------------------
class TEnvironment{
protected:
	size_t _numEnv;
	std::vector<std::string> envNames;

	//parameters on mixture model
	TMCMCParameterPositive _sigma_0;
	TMCMCParameterPositive _rho;
	TMCMCParameterZeroOne _pi;
	TPrior* _model_0;
	TPrior* _model_1;
	TMCMCParameterMixture* _A;

	//prior and updates
	TPrior* prior_pi;
    TPrior* prior_rho;
    TPrior* prior_sigma_0;
    double logPriorProposalRatio;

	//variables to update parameters in turns
	bool hasChanged_A;
	size_t changedIndex_A;
	TMCMCParameter* updatedParameter;

	inline double _calcSigma1FromRhoAndSigma0(){
        return _rho.value + _sigma_0.value;
	}
    inline double _calcRhoFromSigma1(double sigma1){
        return sigma1 - _sigma_0.value;
    }

	virtual void _initialize(int NumEnv);
    double _calcP_AModel_given_pi();
    double _calcP_A_given_sigma0_and_rho();
    double _calcP_A_given_sigma_1();
    void _adjustProposalRanges_A();
    double _calcNewPropKernel(double oldPropKernel, int numUpdates, double meanAccRate);
    double _calcNewPropKernel_A0();
    double _calcNewPropKernel_A1();
    int _fillMeanAcceptanceRate_A0(double & meanAccRate);
    int _fillMeanAcceptanceRate_A1(double & meanAccRate);
    void initializeFromFile();

public:
	TEnvironment();
	TEnvironment(int NumEnv);
	TEnvironment(std::vector<std::string> & Names);
	virtual ~TEnvironment();

	void initialize(std::vector<std::string> & Names);
	void clear();
	void set_pi(double pi){ _pi = pi; };
    void set_rho(double rho);
    void set_sigma_0(double sigma0);
    double get_pi(){ return _pi.value; };
    double get_rho(){ return _rho.value; };
    double get_sigma_0(){ return _sigma_0.value; };
    double get_sigma_1(){ return _calcSigma1FromRhoAndSigma0(); };
    void set_A_zero();
	void set_A(std::vector<double> & A, std::vector<uint8_t> & A_model);
	void setPrior_pi(TPrior* Prior){ prior_pi = Prior; };
    void setPrior_rho(TPrior* Prior){ prior_rho = Prior; };
    void setPrior_sigma_0(TPrior* Prior){ prior_sigma_0 = Prior; };
    void setPrior_A(TPrior* Prior_0, TPrior* Prior_1);
	void setParameters_A();
    void simulate(int NumEnv_nonZero, int NumEnv_zero, double sigma0, double rho, TRandomGenerator* randomGenerator);
	void simulate(int NumEnv_nonZero, int NumEnv_zero, const std::vector<double> & A,  TRandomGenerator* randomGenerator);
	void simulateEnvironmentalVariables(std::vector<double> & vec, TRandomGenerator* randomGenerator);
    double calculatePrior();

	void initializeRho();
    void initializeSigma0();
    void initializeFromFile(std::string filename);

	size_t size(){ return _numEnv; };

	int getIndexFromName(const std::string & name);
	std::string getName(int env){ return envNames[env]; };
	std::string getNames(std::string delim){ return getConcatenatedString(envNames, delim); };
	void addNamesToVector(std::vector<std::string> & vec);
	double getLogPriorProposalRatioLastUpdate(){ return logPriorProposalRatio; };
	std::string getString_A();

	double getLinearCombination(std::vector<double> & environmentalVariables, double & linearComb);
	double updateLinearCombination(std::vector<double> & environmentalVariables, double & linearComb);
	virtual double getParamValue(std::vector<double> & environmentalVariables, double & linearComb){ return getLinearCombination(environmentalVariables, linearComb); };
	virtual double updateParamValue(std::vector<double> & environmentalVariables, double & linearComb){ return getLinearCombination(environmentalVariables, linearComb); };

    void update_pi(TRandomGenerator* randomGenerator);
    void update_rho(TRandomGenerator* randomGenerator);
    void update_sigma_0(TRandomGenerator* randomGenerator);
    bool updateNext_A(TRandomGenerator* randomGenerator);
	void rejectLastUpdate_A();
	virtual void adjustProposalRanges();

	virtual void writeMCMCHeader(gz::ogzstream & out, bool printFullLambdaModel);
	virtual void writeMCMCValues(gz::ogzstream & out, const double LL, const double posterior, bool printFullLambdaModel);
    void writeInclusionProbs(std::ofstream & out);
    virtual void printAcceptanceRates(TLog* logfile, bool estimatePi,  bool estimateRho,  bool estimateSigma0);
};

class TEnvironment_lambda:public TEnvironment{
protected:
	TMCMCParameter _a0;
	TPrior* prior_a0;
	void _initialize(int NumEnv);


public:
	TEnvironment_lambda():TEnvironment(){ prior_a0 = NULL; };
	TEnvironment_lambda(int numEnv):TEnvironment(numEnv){ prior_a0 = NULL; };
	TEnvironment_lambda(std::vector<std::string> & Names):TEnvironment(Names){ prior_a0 = NULL; };

	void set_a0(double a0){ _a0 = a0; };
	void setPrior_a0(TPrior* Prior){ prior_a0 = Prior; };
	void simulate(int NumEnv_nonZero, int NumEnv_zero, double sigma0, double rho, double a0, TRandomGenerator* randomGenerator);
	void simulate(int NumEnv_nonZero, int NumEnv_zero, double a0, const std::vector<double> & A, TRandomGenerator* randomGenerator);
	double getParamValue(std::vector<double> & environmentalVariables, double & linearComb);
	double updateParamValue(std::vector<double> & environmentalVariables, double & linearComb);
	void adjustProposalRanges();
	void update_a0(TRandomGenerator* randomGenerator);
	void reject_a0();
	void writeMCMCHeader(gz::ogzstream & out, bool printFullLambdaModel);
	void writeMCMCValues(gz::ogzstream & out, const double LL, const double posterior, bool printFullLambdaModel);
	void printAcceptanceRates(TLog* logfile, bool estimatePi,  bool estimateRho,  bool estimateSigma0);
    double calculatePrior();
};

class TEnvironment_p:public TEnvironment{
public:
	TEnvironment_p():TEnvironment(){};
	TEnvironment_p(int numEnv):TEnvironment(numEnv){};
	TEnvironment_p(std::vector<std::string> & Names):TEnvironment(Names){};
	double getParamValue(std::vector<double> & environmentalVariables, double & linearComb);
	double updateParamValue(std::vector<double> & environmentalVariables, double & linearComb);
};

#endif /* TENVIRONMENT_H_ */
