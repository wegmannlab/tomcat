/*
 * TEstimator.h
 *
 *  Created on: Dec 16, 2018
 *      Author: phaentu
 */

#ifndef TESTIMATOR_H_
#define TESTIMATOR_H_

#include "TLog.h"
#include "TParameters.h"
#include "TTimeIntervals.h"
#include "TCameraTraps.h"
#include <algorithm>
#include "TMCMCFile.h"

class TEstimator{
private:
	TLog* logfile;
	TRandomGenerator* randomGenerator;
	TEnvironment_lambda env_lambda;
	TEnvironment_p env_p;
	TTimeIntervals* intervals;
	bool intervalsInitialized;
	TCameraTraps* traps;
	bool trapsInitialized;

	std::string trapFileName_lambda;
	std::string trapFileName_p;
	std::string observationFileName;

	std::string outFilename;
	gz::ogzstream mcmcOut_K;
	gz::ogzstream mcmcOut_lambda;
	gz::ogzstream mcmcOut_p;
	bool _printFullLambdaModel;
	bool _writeBurnin;

	std::vector<TPrior*> priors;
	bool estimatePi;
	bool estimateRho;
	bool estimateSigma0;

	void initializeData(TParameters & params);
	void initializeIntervals(TParameters & params);
	void initializeTraps();
	void initializePriors(TParameters & params);
    void calculateLLAndPosteriorFromFileParams(TParameters & params);
    void guessInitialParameters(TParameters & params);
	void guessInitialA(TEnvironment* env, std::string progressString);
	double estimateAPeakFinder(TEnvironment* env, std::vector<double> & A, int index);
	void runMCMC(TParameters & params);
	void runBurnin(int len, int thinning);
	void runMCMC(int len, int thinning);
	void runMCMCIteration();
	void reportAcceptanceRates();
	void openMCMCOutputFile(gz::ogzstream & file, std::string filename);
	void openMCMCOutputFiles();
	void writeCurrentParameters();
    void writeInclusionProbabilities();

public:
	TEstimator(TLog* Logfile, TRandomGenerator* RandomGenerator);
	~TEstimator();

	void runEstimation(TParameters & params);
	void calculatePosteriorFromFile(TParameters & params);
    void readTrapsAndStandardizeEnv(TParameters &params);
};



#endif /* TESTIMATOR_H_ */
