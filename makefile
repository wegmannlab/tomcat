SRC = $(wildcard *.cpp)

OBJ = $(SRC:%.cpp=%.o)
BIN = TomCat

.PHONY : all
all : $(BIN)

# include libraries
BINFLAG = -lz
OBJFLAG = -std=c++14

$(BIN): $(OBJ)
	$(CXX) -O3 -o $(BIN) $(OBJ) $(BINFLAG)

.git/COMMIT_EDITMSG :
	touch $@

%.o: %.cpp
	$(CXX) -O3 -c $(OBJFLAG) $< -o $@


.PHONY : clean
clean:
	rm -rf $(BIN) $(OBJ)

