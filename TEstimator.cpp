/*
 * TEstimator.cpp
 *
 *  Created on: Dec 16, 2018
 *      Author: phaentu
 */

#include "TEstimator.h"

TEstimator::TEstimator(TLog* Logfile, TRandomGenerator* RandomGenerator){
	logfile = Logfile;
	randomGenerator = RandomGenerator;
	trapsInitialized = false;
	traps = NULL;
	intervalsInitialized = false;
	intervals = NULL;
	estimatePi = false;
	estimateRho = false;
	estimateSigma0 = false;
    _printFullLambdaModel = false;
    _writeBurnin = false;
};

TEstimator::~TEstimator(){
	if(intervalsInitialized)
		delete intervals;
	if(trapsInitialized)
		delete traps;
	for(std::vector<TPrior*>::iterator it = priors.begin(); it != priors.end(); ++it)
		delete *it;
};

void TEstimator::initializeIntervals(TParameters & params){
	intervals = new TTimeIntervals(params, logfile);
	intervalsInitialized = true;
};

void TEstimator::initializeTraps(){
	traps = new TCameraTraps(&env_lambda, &env_p, intervals);
	trapsInitialized = true;
};

void TEstimator::runEstimation(TParameters & params){
    // initialize intervals, traps, environmental variables, data and priors
    initializeData(params);

	//estimate initial parameters
	guessInitialParameters(params);

	//run MCMC
	runMCMC(params);
};

void TEstimator::calculatePosteriorFromFile(TParameters &params) {
    // initialize intervals, traps, environmental variables, data and priors
    initializeData(params);

    // read MCMC file and calculate LL and posterior based on these parameters
    calculateLLAndPosteriorFromFileParams(params);
}

void TEstimator::readTrapsAndStandardizeEnv(TParameters &params){
    //read traps
    logfile->startIndent("Reading data:");
    initializeTraps();
    trapFileName_lambda = params.getParameterString("traps_lambda");
    if(trapFileName_lambda == "") throw "Please provide a valid filename with traps_lambda.";
    trapFileName_p = params.getParameterString("traps_p");
    if(trapFileName_p == "") throw "Please provide a valid filename with traps_p.";
    traps->initializeTraps(trapFileName_lambda, trapFileName_p, logfile);

    //standardize environmental variables
    bool standardize = params.getParameterIntWithDefault("standardize", 1);
    if(standardize){
        //get names of files to write mean and variance to
        std::string meanSDFile_lambda = trapFileName_lambda;
        meanSDFile_lambda = extractBeforeLast(meanSDFile_lambda, '.');
        meanSDFile_lambda += "_MeanSD.txt.gz";
        std::string meanSDFile_p = trapFileName_p;
        meanSDFile_p = extractBeforeLast(meanSDFile_p, '.');
        meanSDFile_p += "_MeanSD.txt.gz";

        logfile->listFlush("Normalizing environmental variables ...");
        traps->normalizeEnvVariables(meanSDFile_lambda, meanSDFile_p, logfile);
        logfile->done();
        logfile->conclude("Wrote mean and SD for lambda to file '" + meanSDFile_lambda + "'.");
        logfile->conclude("Wrote mean and SD for p to file '" + meanSDFile_p + "'.");
    }
}

void TEstimator::initializeData(TParameters & params){
    //initialize intervals
    initializeIntervals(params);

    // read traps
    readTrapsAndStandardizeEnv(params);
    traps->setReferenceDate();

    //read data
    observationFileName = params.getParameterString("observations");
    traps->readObservations(observationFileName, logfile);
    logfile->endIndent();

    //read priors
    initializePriors(params);
}

void TEstimator::initializePriors(TParameters & params){
    //output names
    std::string out = params.getParameterString("out", false);
    if(out == ""){
        //construct output from observation file name
        out = observationFileName;
        out = extractBefore(out, '.');
        out += "_";
    }
    outFilename = out;
    logfile->list("Will write output files with prefix '" + outFilename + "'.");

	//read priors
	logfile->startIndent("Reading Priors and proposal kernels:");
	//a0
	double priorSD_a0 = params.getParameterDoubleWithDefault("priorSD_a", 0.1);
	logfile->list("Will use a normal prior on a0 with SD = " + toString(priorSD_a0) + ".");
	priors.emplace_back(new TPriorNormal(0.0, priorSD_a0));
	env_lambda.setPrior_a0(*priors.rbegin());

    //A_lambda mixture
    priors.emplace_back(new TPriorNormal(0.0, 1.)); // sd will be set to corresponding value when sigma_0 is initialized
    priors.emplace_back(new TPriorNormal(0.0, 1.)); // sd will be set to corresponding value when rho is initialized
    env_lambda.setPrior_A(*(priors.rbegin() + 1), *priors.rbegin());

    //B_p mixture
    priors.emplace_back(new TPriorNormal(0.0, 1.)); // sd will be set to corresponding value when sigma_0 is initialized
    priors.emplace_back(new TPriorNormal(0.0, 1.)); // sd will be set to corresponding value when rho is initialized
    env_p.setPrior_A(*(priors.rbegin() + 1), *priors.rbegin());

    //print model of A in output files?
    if (params.parameterExists("printFullLambdaModel"))
        _printFullLambdaModel = true;

	//pi
	//check if we estimate
	if(params.parameterExists("pi_lambda") && params.parameterExists("pi_p")) {
        //pi lambda
        double pi_lambda = params.getParameterDouble("pi_lambda");
        if (pi_lambda <= 0.0 || pi_lambda > 1.0)
            throw "pi_lambda must be within range (0.0, 1.0]!";
        env_lambda.set_pi(pi_lambda);
        logfile->list("Fixing pi_lambda to " + toString(pi_lambda) + ".");

        //pi_p
        double pi_p = params.getParameterDouble("pi_p");
        if(pi_p <= 0.0 || pi_p > 1.0)
            throw "pi_lambda must be within range (0.0, 1.0]!";
        env_p.set_pi(pi_p);
        logfile->list("Fixing pi_p to " + toString(pi_p) + ".");

        estimatePi = false;
    } else if ((params.parameterExists("pi_lambda") && !params.parameterExists("pi_p")) || (!params.parameterExists("pi_lambda") && params.parameterExists("pi_p"))){
	    // check if one of them was given, but the other not -> throw
	    throw "Must specify both pi_lambda and pi_p if pi should be fixed!";
	} else {
		double priorRate_pi = params.getParameterDoubleWithDefault("priorRate_pi", 5);
		logfile->list("Will use an exponential prior on pi with rate = " + toString(priorRate_pi) + ".");
		priors.emplace_back(new TPriorExponential(priorRate_pi));
		env_lambda.setPrior_pi(*priors.rbegin());
		env_p.setPrior_pi(*priors.rbegin());
		env_lambda.set_pi(0.1); // just some value, think of something smarter!
        env_p.set_pi(0.1);
        estimatePi = true;
	}

    // sigma_0 (mixture model)
    // check if we estimate
    if(params.parameterExists("sigma_0_lambda") && params.parameterExists("sigma_0_p")) {
        //sigma_0_lambda
        double sigma_0_lambda = params.getParameterDouble("sigma_0_lambda");
        if (sigma_0_lambda <= 0.)
            throw "sigma_0_lambda must be larger than 0.!";
        env_lambda.set_sigma_0(sigma_0_lambda);
        logfile->list("Fixing sigma_0_lambda to " + toString(sigma_0_lambda) + ".");

        //sigma_0_p
        double sigma_0_p = params.getParameterDouble("sigma_0_p");
        if (sigma_0_p <= 0.)
            throw "sigma_0_p must be larger than 0.!";
        env_p.set_sigma_0(sigma_0_p);
        logfile->list("Fixing sigma_0_p to " + toString(sigma_0_p) + ".");

        estimateSigma0 = false;
    } else if ((params.parameterExists("sigma_0_lambda") && !params.parameterExists("sigma_0_p")) || (!params.parameterExists("sigma_0_lambda") && params.parameterExists("sigma_0_p"))){
        // check if one of them was given, but the other not -> throw
        throw "Must specify both sigma_0_lambda and sigma_0_p if sigma_0 should be fixed!";
    } else {
        double priorRate_sigma_0 = params.getParameterDoubleWithDefault("priorRate_sigma_0", 1000);
        if (priorRate_sigma_0 <= 0)
            throw "priorRate_sigma_0 must be larger than 0.!";
        logfile->list(
                "Will use an exponential prior on sigma_0 with a rate = " + toString(priorRate_sigma_0) + ".");
        priors.emplace_back(new TPriorExponential(priorRate_sigma_0));
        env_lambda.setPrior_sigma_0(*priors.rbegin());
        env_p.setPrior_sigma_0(*priors.rbegin());
        estimateSigma0 = true;
    }

    // rho_lambda (mixture model on A)
    // check if we estimate
    if(params.parameterExists("rho_lambda") && params.parameterExists("rho_p")) {
        //rho_lambda
        double rho_lambda = params.getParameterDouble("rho_lambda");
        if(rho_lambda <= 0.)
            throw "rho_lambda must be larger than 0.!";
        env_lambda.set_rho(rho_lambda);
        logfile->list("Fixing rho_lambda to " + toString(rho_lambda) + ".");

        //rho_p
        double rho_p = params.getParameterDouble("rho_p");
        if(rho_p <= 0.)
            throw "rho_p must be larger than 0.!";
        env_p.set_rho(rho_p);
        logfile->list("Fixing rho_p to " + toString(rho_p) + ".");

        estimateRho = false;
    } else if ((params.parameterExists("rho_lambda") && !params.parameterExists("rho_p")) || (!params.parameterExists("rho_lambda") && params.parameterExists("rho_p"))){
        // check if one of them was given, but the other not -> throw
        throw "Must specify both rho_lambda and rho_p if rho should be fixed!";
    } else{
        double priorRate_rho = params.getParameterDoubleWithDefault("priorRate_rho", 2);
        if (priorRate_rho <= 0)
            throw "priorRate_rho must be larger than 0.!";
        logfile->list(
                "Will use an exponential prior on rho with a rate = " + toString(priorRate_rho) + ".");
        priors.emplace_back(new TPriorExponential(priorRate_rho));
        env_lambda.setPrior_rho(*priors.rbegin());
        env_p.setPrior_rho(*priors.rbegin());
        estimateRho = true;
    }

    logfile->endIndent();
};

void TEstimator::calculateLLAndPosteriorFromFileParams(TParameters & params){
    // read parameters
    std::string mcmcFilename_lambda = params.getParameterString("mcmc_lambda");
    if(mcmcFilename_lambda.empty()) throw "Please provide a valid filename with mcmc_lambda.";
    std::string mcmcFilename_p = params.getParameterString("mcmc_p");
    if(mcmcFilename_p.empty()) throw "Please provide a valid filename with mcmc_p.";
    logfile->listFlush("Initializing environments from MCMC output '" + mcmcFilename_lambda + "' and '" + mcmcFilename_p + "' ...");

    // open MCMC files
    TMCMCFile_lambda mcmc_lambda(mcmcFilename_lambda);
    TMCMCFile mcmc_p(mcmcFilename_p);

    // check if both files contain models of A
    mcmc_lambda.checkForAModelInHeader();
    if (!mcmc_lambda.withAModel())
        throw "MCMC file '" + mcmcFilename_lambda + "' must contain models of A in order to compute the posterior! Run your MCMC inference again with option 'printFullLambdaModel'.";

    mcmc_p.checkForAModelInHeader();
    if (!mcmc_p.withAModel())
        throw "MCMC file '" + mcmcFilename_p + "' must contain models of A in order to compute the posterior! Run your MCMC inference again with option 'printFullLambdaModel'.";

    // now set values
    mcmc_lambda.readNext();
    mcmc_p.readNext();
    mcmc_lambda.setValues(env_lambda);
    mcmc_p.setValues(env_p);
    traps->update();

    // calculate posterior
    double LL = traps->getLL();
    double posterior = LL + env_lambda.calculatePrior() + env_p.calculatePrior();

    // finally write file with posterior and LL
    _printFullLambdaModel = true;
    gz::ogzstream LLPost_lambda;
    gz::ogzstream LLPost_p;
    openMCMCOutputFile(LLPost_lambda, outFilename + "posterior_lambda.txt.gz");
    openMCMCOutputFile(LLPost_p, outFilename + "posterior_p.txt.gz");
    env_lambda.writeMCMCHeader(LLPost_lambda, _printFullLambdaModel);
    env_p.writeMCMCHeader(LLPost_p, _printFullLambdaModel);
    env_lambda.writeMCMCValues(LLPost_lambda, LL, posterior, _printFullLambdaModel);
    env_p.writeMCMCValues(LLPost_p, LL, posterior, _printFullLambdaModel);
    LLPost_lambda.close();
    LLPost_p.close();
}

void TEstimator::guessInitialParameters(TParameters & params){
	logfile->startIndent("Guessing initial parameters:");
	//estimate K
	traps->guessK();
	logfile->list("K = " + intervals->getKString() + ".");

	//guess average lambda from fraction of intervals with data assuming p=0.5
	//Note: lambdaBar has units 1/day
	double fracWithObs = traps->getFractionOfIntervalsWithObservations(); // fraction of intervals that have >0 observations
	double lambdaBar = -2.0 * log(1.0 - fracWithObs)
	        / intervals->getObservationIntervalLength() * intervals->getNumSecondsPerDay(); // to get rate per day
	double a0_lambda = log(lambdaBar);

	env_lambda.set_a0(a0_lambda);
	logfile->list("Estimated lambdaBar = " + toString(lambdaBar) + ", implying a0_lambda = " + toString(a0_lambda) + ".");

	//guess A_lambda, then A_p
	guessInitialA(&env_lambda, "Guessing initial A_lambda");
	guessInitialA(&env_p, "Guessing initial A_p");

	// guess sigma0 and rho
	if (estimateSigma0) {
        env_lambda.initializeSigma0();
        env_p.initializeSigma0();
    }
	if (estimateRho){
        env_lambda.initializeRho();
        env_p.initializeRho();
	}

    //now update traps
	traps->update();
	logfile->list("Initial LL = " + toString(traps->getLL()) + ".");

    logfile->endIndent();
};

void TEstimator::guessInitialA(TEnvironment* env, std::string progressString){
	std::vector<double> A(env->size(), 0.0);
    std::vector<uint8_t> A_model(env->size(), 0);
    std::vector<bool> A_was_set(env->size(), false);

	//iteratively find highest A with the highest LL given all previously found
	int numAToFind = std::floor(env->get_pi() * env->size()); // pi percent of A should be in 1-model

	for(int i=0; i<env->size(); i++){
		logfile->listOverFlush(progressString + " ... (" + toString(round(100 * (double) i / (double) env->size())) + "%)");

		//fill vector with estimates of A and their LL for those not yet set
		std::vector<double> peakA(env->size(), 0.0);
        std::vector<double> LL_of_peakA(env->size(), 0.0);
        for(size_t e=0; e<env->size(); e++){
            std::vector<double> tmpA = A;
            if(!A_was_set[e]) {
                LL_of_peakA[e] = estimateAPeakFinder(env, tmpA, e);
                peakA[e] = tmpA[e]; // store, as tmpA will be cleared in each iteration
            }
        }

		//find A with the highest LL not yet set
		double max = std::numeric_limits<double>::lowest();
		int maxIndex = 0;
		for(size_t e=0; e<env->size(); e++){
			if(!A_was_set[e] && LL_of_peakA[e] > max){
				max = LL_of_peakA[e];
				maxIndex = e;
			}
		}

        //set A with the highest LL
        A[maxIndex] = peakA[maxIndex];
        A_was_set[maxIndex] = true;
        if (std::count(A_model.begin(), A_model.end(), 1) < numAToFind)
            A_model[maxIndex] = 1; // only first pi% of A's should be in 1-model, although we set all of them
	}

	logfile->overList(progressString + " ... done!   ");

	// construct nice logfile string
	std::string out = "Set A to 1-model for these environmental variables: ";
	bool first = true;
	for(size_t e=0; e<env->size(); e++){
		if(A_model[e] == 1){
			if(first) first = false;
			else out += ", ";
			out += env->getName(e);
		}
	}
	logfile->conclude(out);

	//now set estimated A
	env->set_A(A, A_model);
};

double TEstimator::estimateAPeakFinder(TEnvironment* env, std::vector<double> & A, int index){
	A[index] = 0.0;
    std::vector<uint8_t> A_model(env->size(), 0); // model of A does not matter for LL, just set all to 0!
	env->set_A(A, A_model);
	double LL = traps->update();

	//run stupid 1D search to find peak
	double step = 0.1;
	for(int s=0; s<50; s++){
		A[index] += step;
		env->set_A(A, A_model);
		double newLL = traps->update();
		if(newLL < LL) //switch if LL decreases
			step = - step / 2.718282;

		LL = newLL;
	}

	return LL;
};

void TEstimator::runMCMC(TParameters & params){
	logfile->startIndent("Running MCMC inference:");

	//reading MCMC parameters
	//iterations
	logfile->startIndent("Reading MCMC parameters:");
	int iterations = params.getParameterIntWithDefault("iterations", 100000);
	logfile->list("Will run an MCMC for " + toString(iterations) + " iterations.");

	//burnin
	int burnin  = params.getParameterIntWithDefault("burnin", 1000);
	int numBurnin = params.getParameterIntWithDefault("numBurnin", 10);
	if(numBurnin > 0)
		logfile->list("Will run " + toString(numBurnin) + " burnins of " + toString(burnin) + " iterations each.");

	//thinning
	int thinning = params.getParameterIntWithDefault("thinning", 1);
	if(thinning < 1) throw "Thinning must be > 0!";
	if(thinning == 1)
		logfile->list("Will write full chain.");
	else if(thinning == 2)
		logfile->list("Will write every second iteration.");
	else if(thinning == 3)
		logfile->list("Will write every third iteration.");
	else
		logfile->list("Will write every " + toString(thinning) + "th iteration.");

	// write during burnin?
	if (params.parameterExists("writeBurnin")) {
        _writeBurnin = true;
        logfile->list("Will write traces during burnin too.");
    }
    logfile->endIndent();

    //open MCMC file
	openMCMCOutputFiles();

	//run burnin
	if(numBurnin > 0){
		if(numBurnin > 1)
			logfile->startNumbering("Running " + toString(numBurnin) + " burnins:");
		else
			logfile->startNumbering("Running " + toString(numBurnin) + " burnin:");
		for(int b=0; b<numBurnin; ++b){
			logfile->number("Burnin number " + toString(b+1) + ":");
			logfile->addIndent(2);
			runBurnin(burnin, thinning);
			logfile->removeIndent(2);
		}
		logfile->endIndent();
	}

	//run MCMC chain
	logfile->startIndent("Running MCMC chain:");
	runMCMC(iterations, thinning);
	logfile->endIndent();

	//close files
	mcmcOut_K.close();
	mcmcOut_lambda.close();
	mcmcOut_p.close();
};

void TEstimator::runBurnin(int len, int thinning){
	std::string report = "Running a burnin of " + toString(len) + " iterations ... ";
	logfile->listFlush(report + "(0%)");

	int prog = 0;
	int prog_old = 0;

    if (_writeBurnin)
        writeCurrentParameters();

	for(int i=0; i<len; ++i){
		runMCMCIteration();

        //print to file?
        if(i % thinning == 0 && _writeBurnin)
            writeCurrentParameters();

        //report progress
		prog = 100 * (double) i / (double) len;
		if(prog > prog_old){
			logfile->listOverFlush(report + "(" + toString(prog) + "%)");
			prog_old = prog;
		}
	}
	logfile->overList(report + "done! ");

	//report acceptance rates
	reportAcceptanceRates();

	//adjust proposal ranges
	logfile->listFlush("Adjusting proposal parameters ...");
	env_lambda.adjustProposalRanges();
	env_p.adjustProposalRanges();
	intervals->adjustProposalRanges();
	logfile->done();
};

void TEstimator::runMCMC(int len, int thinning){
	std::string report = "Running an MCMC chain of " + toString(len) + " iterations ... ";
	logfile->listFlush(report + "(0%)");

	int prog = 0;
	int prog_old = 0;
	for(int i=0; i<len; ++i){
		runMCMCIteration();

		//print to file?
		if(i % thinning == 0)
			writeCurrentParameters();

		//report progress
		prog = 100 * (double) i / (double) len;
		if(prog > prog_old){
			logfile->listOverFlush(report + "(" + toString(prog) + "%)");
			prog_old = prog;
		}
	}
	logfile->overList(report + "done! ");

	//report acceptance rates
	reportAcceptanceRates();

	// write inclusion probabilities
	writeInclusionProbabilities();
};

void TEstimator::runMCMCIteration(){
	//update all environmental parameters for lambda bar
	while(env_lambda.updateNext_A(randomGenerator)){
		if(!traps->updateLambdaBar(randomGenerator))
			env_lambda.rejectLastUpdate_A();
	}

	//update all environmental parameters for p
	while(env_p.updateNext_A(randomGenerator)){
		if(!traps->updateP(randomGenerator))
			env_p.rejectLastUpdate_A();
	}

	//update hierarchical parameters (a0 pi, rho, sigma_0)
	env_lambda.update_a0(randomGenerator);
	if(!traps->updateLambdaBar(randomGenerator))
		env_lambda.reject_a0();

	if(estimatePi){
		env_lambda.update_pi(randomGenerator);
		env_p.update_pi(randomGenerator);
	}
	if (estimateRho) {
        env_lambda.update_rho(randomGenerator);
        env_p.update_rho(randomGenerator);
    }
	if (estimateSigma0) {
        env_lambda.update_sigma_0(randomGenerator);
        env_p.update_sigma_0(randomGenerator);
    }

	//update intervals K and shift
	while(intervals->updateNextK(randomGenerator)){
		if(!traps->updateK(randomGenerator))
			intervals->rejectLastUpdate_K();
	}

	intervals->updateShift(randomGenerator);
	if(!traps->updateK(randomGenerator))
		intervals->rejectShitUpdate();
};

void TEstimator::reportAcceptanceRates(){
	logfile->startIndent("Acceptance rates lambda:");
	env_lambda.printAcceptanceRates(logfile, estimatePi, estimateRho, estimateSigma0);
	logfile->endIndent();

	logfile->startIndent("Acceptance rates p:");
	env_p.printAcceptanceRates(logfile, estimatePi, estimateRho, estimateSigma0);
	logfile->endIndent();

	logfile->startIndent("Acceptance rates K:");
	intervals->printAcceptanceRates(logfile);
	logfile->endIndent();
};

void TEstimator::openMCMCOutputFile(gz::ogzstream & file, std::string filename){
	file.open(filename.c_str());
	if(!file) throw "Failed to open file '" + filename + "' for writing";
};

void TEstimator::openMCMCOutputFiles(){
	//open file for K
	openMCMCOutputFile(mcmcOut_K, outFilename + "MCMC_K.txt.gz");
	intervals->writeMCMCHeader(mcmcOut_K);

	//open file for lambda
	openMCMCOutputFile(mcmcOut_lambda, outFilename + "MCMC_lambda.txt.gz");
	env_lambda.writeMCMCHeader(mcmcOut_lambda, _printFullLambdaModel);

	//open file for p
	openMCMCOutputFile(mcmcOut_p, outFilename + "MCMC_p.txt.gz");
	env_p.writeMCMCHeader(mcmcOut_p, _printFullLambdaModel);
};

void TEstimator::writeCurrentParameters(){
	intervals->writeMCMCValues(mcmcOut_K, traps->getLL());

	double LL = traps->getLL();
	double posterior = env_lambda.calculatePrior() + env_p.calculatePrior() + LL;

	env_lambda.writeMCMCValues(mcmcOut_lambda, LL, posterior, _printFullLambdaModel);
	env_p.writeMCMCValues(mcmcOut_p, LL, posterior, _printFullLambdaModel);
};

void TEstimator::writeInclusionProbabilities(){
    // open file
    std::ofstream file_lambda(outFilename + "MCMC_inclusionProbs_lambda.txt.gz");
    std::ofstream file_p(outFilename + "MCMC_inclusionProbs_p.txt.gz");

    env_lambda.writeInclusionProbs(file_lambda);
    env_p.writeInclusionProbs(file_p);

    file_lambda.close();
    file_p.close();
}
