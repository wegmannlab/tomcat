/*
 * TSimulator.h
 *
 *  Created on: Dec 15, 2018
 *      Author: phaentu
 */

#ifndef TSIMULATOR_H_
#define TSIMULATOR_H_


#include "TLog.h"
#include "TParameters.h"
#include "TTimeIntervals.h"
#include "TCameraTraps.h"
#include "TEnvironment.h"
#include "TFile.h"

class TSimulator{
private:
	TLog* logfile;

	void simulateLocations(const std::string & out, const int & numLocations, TEnvironment_lambda & env_lambda, TRandomGenerator* randomGenerator);
    void writeTrueParamsMixedModel(const std::string & out, TEnvironment & env, const std::string& whichEnv);
    void writeTrueK(const std::string & out, TTimeIntervals & intervals);

public:
	TSimulator(TLog* Logfile);

	void runSimulations(TParameters & params, TRandomGenerator* randomGenerator);

};




#endif /* TSIMULATOR_H_ */
