/*
 * TMeanVar.h
 *
 *  Created on: Mar 24, 2019
 *      Author: phaentu
 */

#ifndef TMEANVAR_H_
#define TMEANVAR_H_

#include <math.h>

//--------------------------------------------
// TMeanVar
//--------------------------------------------
class TMeanVar{
private:
	double _sum;
	double _sumOfSquares;
	int _size;
	double _mean;
	double _var;
	double _sd;

	bool _meanVarCalculated;

public:

	TMeanVar(){
		clear();
	};

	void clear(){
		_sum = 0.0;
		_sumOfSquares = 0.0;
		_mean = 0.0;
		_var = 1.0;
		_sd = 1.0;
		_size = 0;
		_meanVarCalculated = false;
	};

	double mean(){
		if(!_meanVarCalculated)
			calcMeanVar();
		return _mean;
	};
	double sd(){
		if(!_meanVarCalculated)
			calcMeanVar();
		return _sd;
	};
	double var(){
		if(!_meanVarCalculated)
			calcMeanVar();
		return _var;
	};

	int size(){
		return _size;
	};

	void set(const double & Mean, const double & Var){
		_mean = Mean;
		_var = Var;
		_sd = sqrt(_var);
		_meanVarCalculated = true;
	};

	void setMeanSD(const double & Mean, const double & SD){
		_mean = Mean;
		_sd = SD;
		_var = _sd * _sd;
		_meanVarCalculated = true;
	};

	void add(const double & val){
		_sum += val;
		_sumOfSquares += val * val;
		++_size;
	};

	void calcMeanVar(){
		_mean = _sum / (double) _size;
		_var = _sumOfSquares / (double) _size - _mean * _mean;
		_sd = sqrt(_var);
		_meanVarCalculated = true;
	};
};

#endif /* TMEANVAR_H_ */
