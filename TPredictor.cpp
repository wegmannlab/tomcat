/*
 * TProjector.cpp
 *
 *  Created on: Jan 7, 2019
 *      Author: phaentu
 */


#include "TPredictor.h"

TLocation::TLocation(std::string Coor1, std::string Coor2, std::vector<double> & EnvValues, TMeanVar* mv, TEnvironment* Env){
	coor1 = Coor1;
	coor2 = Coor2;
	envValues.init(EnvValues, Env, mv);

	//store first value
	densities.push_back(envValues.value);
};

double TLocation::getDensity(TEnvironment* Env){
	envValues.update(Env);
	return envValues.value;
};

void TLocation::addDensity(TEnvironment* Env){
	envValues.update(Env);
	densities.push_back(envValues.value);
};

void TLocation::writePosteriorDensities(TOutputFile* out){
	//coor1, coor2
	*out << coor1 << coor2 << envValues.getMostExtremeValue();

	//mean
	double mean = 0.0;
	for(std::vector<double>::iterator it=densities.begin(); it!=densities.end(); ++it)
		mean += *it;
	mean /= (double) densities.size();
	*out << mean;

	//median
	sort(densities.begin(), densities.end());
	int index = round(densities.size() / 2.0);
	*out << densities[index];

	//5% and 95% quantile
	index = round(densities.size() * 0.05);
	*out << densities[index];
	index = round(densities.size() * 0.95);
	*out << densities[index];

	*out << std::endl;
};

//---------------------------------------
// TPredictorEnvironment
//---------------------------------------
TPredictorEnvironment::TPredictorEnvironment(){
	mv = NULL;
	mvInitialized = false;
	envIndex = NULL;
	envIndexInitialized = false;
};

TPredictorEnvironment::TPredictorEnvironment(std::string mcmcFileName, std::string stdFileName, bool mcmcFileIsOneLine, TLog* logfile){
	TPredictorEnvironment();
	initialize(mcmcFileName, stdFileName, mcmcFileIsOneLine, logfile);
};

TPredictorEnvironment::~TPredictorEnvironment(){
	if(mvInitialized)
		delete[] mv;
	if(envIndexInitialized)
		delete[] envIndex;
};

void TPredictorEnvironment::initialize(std::string mcmcFileName, std::string stdFileName, bool mcmcFileIsOneLine, TLog* logfile){
	//initialize environment
	logfile->listFlush("Initializing environment from MCMC output '" + mcmcFileName + "' ...");
	initEnvironment(mcmcFileName, mcmcFileIsOneLine);
	logfile->done();
	logfile->conclude("Read " + toString(env_lambda.size()) + " environmental variables.");

	//read values to standardize
	logfile->listFlush("Reading mean and SD to standardize environmental variables from '" + stdFileName + "' ...");
	readStandardization(stdFileName);
	logfile->done();
};

void TPredictorEnvironment::initEnvironment(std::string & filename, bool mcmcFileIsOneLine){
	//open MCMC file
	if (mcmcFileIsOneLine)
        mcmcFile_lambda = std::make_unique<TMCMCFile_lambda_oneLine>(filename);
	else
        mcmcFile_lambda = std::make_unique<TMCMCFile_lambda>(filename);
	mcmcFile_lambda->checkForAModelInHeader();

	// check if both files contain models of A
    mcmcFile_lambda->checkForAModelInHeader();
    if (!mcmcFile_lambda->withAModel())
        throw "MCMC file '" + filename + "' must contain models of A in order to compute the posterior! Run your MCMC inference again with option 'printFullLambdaModel'.";

	//extract environment names from MCMC header: remove "A_" in front!
	std::vector<std::string> envNames;
	for(std::vector<std::string>::iterator it=mcmcFile_lambda->header.begin() + 6; it!=mcmcFile_lambda->header.end(); it++){
	    if (stringContains(*it, "A_model_")) {
	        // break as soon as A-model appears
            break;
        }
		std::string name = extractAfter(*it, "A_");
		envNames.push_back(name);
	}

	//initialize environment
	env_lambda.initialize(envNames);
};

void TPredictorEnvironment::readStandardization(std::string & filename){
	//create storage
	mv = new TMeanVar[env_lambda.size()];
	mvInitialized = true;
	bool* mv_set = new bool[env_lambda.size()];
	for(size_t i=0; i<env_lambda.size(); ++i)
		mv_set[i] = false;

	//open file
	gz::igzstream file(filename.c_str());
	if(!file)
		throw "Failed to open file '" + filename + "' for reading!";

	//skip header
	std::vector<std::string> vec;
	std::string line;
	getline(file, line);

	//now parse file
	int lineNum = 1;
	while(file.good() && !file.eof()){
		++lineNum;
		getline(file, line);
		fillVectorFromStringWhiteSpaceSkipEmpty(line, vec);

		//skip empty lines
		if(vec.size() == 0) continue;

		//check if there are three columns: env, mean, sd
		if(vec.size() != 3)
			throw "Wrong number of columns in file '" + filename + "' on line " + toString(lineNum) + "! Expected 3 (name, mean, SD), but found " + toString(vec.size()) + ".";

		//check if env variale is used
		int index = env_lambda.getIndexFromName(vec[0]);
		if(index < 0) continue;

		//save values
		mv[index].setMeanSD(stringToDouble(vec[1]), stringToDouble(vec[2]));
		mv_set[index] = true;
	}

	//check if all mv were set
	for(size_t i=0; i<env_lambda.size(); ++i){
		if(!mv_set[i]){
			throw "No mean and SD provided for variable '" + env_lambda.getName(i) + "' in file '" + filename + "'!";
		}
	}

	//clean up
	delete[] mv_set;
	file.close();
};

void TPredictorEnvironment::matchEnvironmentToLocations(std::vector<std::string> & locationHeader){
	//match env variables
	envIndex = new int[env_lambda.size()];
	for(size_t i=0; i<env_lambda.size(); ++i)
		envIndex[i] = -1;

	for(size_t i=2; i<locationHeader.size(); ++i){
		int index = env_lambda.getIndexFromName(locationHeader[i]);
		if(index >= 0){ //variable exists
			envIndex[index] = i;
		}
	}

	envIndexInitialized = true;

	//make sure all env variables are present in locations file
	for(size_t i=0; i<env_lambda.size(); ++i){
		if(envIndex[i] < 0)
			throw "Environmental variable '" + env_lambda.getName(i) + "' not provided in locations file!";
	}
};

void TPredictorEnvironment::addLocation(std::vector<std::string> & locationLine){
	//prepare tmp variables
	std::vector<double> values;
	values.resize(env_lambda.size());

	//fill vector of values
	for(size_t i=0; i<env_lambda.size(); ++i){
		values[i] = stringToDouble(locationLine[envIndex[i]]);
	}

	//create location
	locations.emplace_back(locationLine[0], locationLine[1], values, mv, &env_lambda);
};

int TPredictorEnvironment::getNumLocationsWithExtremValues(){
	int numLocationsWithExtremeValues = 0;

	for(TLocation it : locations){
		if(it.getMostExtremeValue() > 5.0)
			++numLocationsWithExtremeValues;
	}

	return numLocationsWithExtremeValues;
};

void TPredictorEnvironment::predictFromMCMCFile(TLog* logfile, int limitLines){
	//loop over MCMC file
	logfile->startIndent("Predicting densities from up to " + toString(limitLines) + " posterior samples:");
	logfile->listOverFlush("Parsed 0 posterior samples ...");

	while(mcmcFile_lambda->getNumSamplesRead() < limitLines && mcmcFile_lambda->readNext()){
		//update environment
		mcmcFile_lambda->setValues(env_lambda);

		//calculate densities
		for(std::vector<TLocation>::iterator it=locations.begin(); it!=locations.end(); ++it){
			it->addDensity(&env_lambda);
		}

		//report
		if(mcmcFile_lambda->getNumSamplesRead() % 1000 == 0)
			logfile->listOverFlush("Parsed " + toString(mcmcFile_lambda->getNumSamplesRead()) + " posterior samples ...");
	}
	logfile->overList("Parsed " + toString(mcmcFile_lambda->getNumSamplesRead()) + " posterior samples ... reached end of file.");
	logfile->endIndent();
};

void TPredictorEnvironment::writePosteriorDensities(const std::string & coor1Name, const std::string & coor2Name, std::string & filename){
	//open file
	TOutputFileZipped out(filename);
	out.writeHeader({coor1Name, coor2Name, "maxSD", "mean", "median", "quantile_0.05", "quantile_0.95"});

	//write locations
	for(std::vector<TLocation>::iterator it=locations.begin(); it!=locations.end(); ++it)
		it->writePosteriorDensities(&out);
};

bool TPredictorEnvironment::fillVectorOfNormalizedDensitiesForNextMCMCSample(std::vector<double> & densities){
	if(!mcmcFile_lambda->readNext())
		return false;

	//update environment
	mcmcFile_lambda->setValues(env_lambda);

	//calculate densities
	densities.resize(locations.size());
	double sum = 0.0;
	for(size_t i = 0; i<locations.size(); ++i){
		densities[i] = locations[i].getDensity(&env_lambda);
		sum += densities[i];
	}

	//normalize
	for(size_t i = 0; i<locations.size(); ++i)
		densities[i] /= sum;

	return true;
};

void TPredictorEnvironment::addNamesToVector(std::vector<std::string> & vec){
    env_lambda.addNamesToVector(vec);
};

void TPredictorEnvironment::simulateEnvironmentalVariables(std::vector<double> & vec, TRandomGenerator* randomGenerator){
    env_lambda.simulateEnvironmentalVariables(vec, randomGenerator);
};

//---------------------------------------
// TProjector_base
//---------------------------------------
TPredictor_base::TPredictor_base(TLog* Logfile){
	logfile = Logfile;
};

void TPredictor_base::addLocationsToEnvironments(std::vector<std::string> & vec){
	env.addLocation(vec);
};

void TPredictor_base::matchEnvironmentToLocations(std::vector<std::string> & vec){
	env.matchEnvironmentToLocations(vec);
};

void TPredictor_base::_simulateLocationsOnTheFly(TParameters & params, const std::string& outPrefix){
    // for stupid people like me who forget to simulate locations but need a locations-file
    // attention: this is only a hack and should not be used; instead simulate with locations!
    int numLocations = params.getParameterInt("simulateLocations");
    if (numLocations < 1)
        throw "Specify > 1 locations when using parameter 'simulateLocations!'";
    logfile->listFlush("Simulating locations ...");

    // initialize randomGenerator
    TRandomGenerator randomGenerator;

    //open locus file and add header
    TOutputFileZipped locFile(outPrefix + "locations.txt.gz");
    std::vector<std::string> header = {"coor1", "coor2"};
    env.addNamesToVector(header);
    locFile.writeHeader(header);

    //simulate locations
    std::vector<double> envVar;
    double linearComb;
    for(int i=0; i<numLocations; ++i){
        //simulate environmental variables and get density
        env.simulateEnvironmentalVariables(envVar, &randomGenerator);

        //write locations
        locFile << i << i;
        locFile.write(envVar);
        locFile.endLine();
    }

    locFile.close();
    logfile->done();
}

void TPredictor_base::readLocations(TParameters & params, const std::string& outPrefix){
    if (params.parameterExists("simulateLocations")){
        _simulateLocationsOnTheFly(params, outPrefix);
        locationsFileName = outPrefix + "locations.txt.gz";
    } else {
        locationsFileName = params.getParameterString("locations");
    }

	//open file
	logfile->listFlush("Reading locations from file '" + locationsFileName + "' ...");
	if(locationsFileName.empty()) throw "Please provide a valid filename with locations.";
	gz::igzstream file(locationsFileName.c_str());
	if(!file)
		throw "Failed to open file '" + locationsFileName + "' for reading!";

	//read header
	std::vector<std::string> vec;
	std::string line;
	getline(file, line);
	fillVectorFromStringWhiteSpaceSkipEmpty(line, vec);

	//store names of coordinate columns
	coor1Name = vec[0];
	coor2Name = vec[1];

	//match env variables
	matchEnvironmentToLocations(vec);

	//prepare tmp variables
	int lineNum = 1;
	size_t numCols = vec.size();
	int numLocations = 0;

	//read locations
	while(file.good() && !file.eof()){
		++lineNum;
		getline(file, line);
		fillVectorFromStringWhiteSpaceSkipEmpty(line, vec);

		//skip empty lines
		if(vec.size() == 0) continue;

		//check if there are three columns: env, mean, sd
		if(vec.size() != numCols)
			throw "Wrong number of columns in file '" + locationsFileName + "' on line " + toString(lineNum) + "! Expected " + toString(numCols) + " but found " + toString(vec.size()) + ".";

		//add location
		addLocationsToEnvironments(vec);
		++numLocations;
	}

	//report
	logfile->done();
	logfile->conclude("Read " + toString(numLocations) + " locations.");

	//clean up
	file.close();

	//check for extreme values
	int numLocationsWithExtremeValues = env.getNumLocationsWithExtremValues();
	if(numLocationsWithExtremeValues > 0)
		logfile->warning(toString(numLocationsWithExtremeValues) + " locations had values that were > 5 standard deviations away from the mean!");
};

//---------------------------------------
// TPredictor
//---------------------------------------
void TPredictor::predict(TParameters & params){
	//initialize environment
	std::string mcmcFilename = params.getParameterString("mcmc_lambda");
	if(mcmcFilename.empty()) throw "Please provide a valid filename with mcmc_lambda.";
	std::string stdFilename = params.getParameterString("std_lambda");
	if(stdFilename.empty()) throw "Please provide a valid filename with std_lambda.";

	env.initialize(mcmcFilename, stdFilename, false, logfile);

    //output names
    std::string out = params.getParameterString("out", false);
    if(out == ""){
        //construct output from observation file name
        out = locationsFileName; // is location file
        out = extractBefore(out, '.');
        out += "_";
    }
    logfile->list("Will write output files with prefix '" + out + "'.");


    //read locations
	readLocations(params, out);

	//limit lines
	int limitLines = params.getParameterIntWithDefault("limitLines", 100000000);

	//project for each entry in the MCMC file
	env.predictFromMCMCFile(logfile, limitLines);

	//calculate and write posterior characteristics
	std::string filename = out + "posteriorDensities.txt.gz";
	logfile->listFlush("Will write posterior characteristics to file '" + filename + "' ...");
	env.writePosteriorDensities(coor1Name, coor2Name, filename);
	logfile->done();
};

//---------------------------------------
// TDeltaEEstimator
//---------------------------------------
std::string TDeltaSEstimator::openEnvironments(TParameters & params){
	//initialize environment 1
	logfile->startIndent("Initializing environment 1:");
	bool env1IsSimulatedParams = false;
	std::string mcmcFilename1 = params.getParameterString("mcmc_lambda");
	if(mcmcFilename1.empty()) throw "Please provide a valid filename with mcmc_lambda.";
	std::string stdFilename1 = params.getParameterString("std_lambda");
	if(stdFilename1.empty()) throw "Please provide a valid filename with std_lambda.";
	if (params.parameterExists("mcmc_lambda_truth")) {
        env1IsSimulatedParams = true;
        logfile->list("Will treat file " + mcmcFilename1 + " as a truth file containing simulated values on one line.");
    }

	env.initialize(mcmcFilename1, stdFilename1, env1IsSimulatedParams, logfile);
	logfile->endIndent();

	//initialize environment 2
	logfile->startIndent("Initializing environment 2:");
    bool env2IsSimulatedParams = false;
    std::string mcmcFilename2 = params.getParameterString("mcmc_lambda_2");
	if(mcmcFilename2.empty()) throw "Please provide a valid filename with mcmc_lambda_2.";
	std::string stdFilename2 = params.getParameterString("std_lambda_2");
	if(stdFilename2.empty()) throw "Please provide a valid filename with std_lambda_2.";
    if (params.parameterExists("mcmc_lambda_2_truth")) {
        logfile->list("Will treat file " + mcmcFilename2 + " as a truth file containing simulated values on one line.");
        env2IsSimulatedParams = true;
    }

    env2.initialize(mcmcFilename2, stdFilename2, env2IsSimulatedParams, logfile);
	logfile->endIndent();

	//set meanVar to zero
	meanVar.clear();

	//assemble output prefix
	std::string outPrefix = params.getParameterString("out", false);
	if(outPrefix == ""){
		//construct output from observation file name
		outPrefix = mcmcFilename1;
		outPrefix = extractBefore(outPrefix, '.');
		std::string tmp = mcmcFilename2;
		tmp = extractBefore(tmp, '.');
		outPrefix += "_" + tmp;
	}
	return outPrefix;
};

void TDeltaSEstimator::addLocationsToEnvironments(std::vector<std::string> & vec){
	env.addLocation(vec);
	env2.addLocation(vec);
};

void TDeltaSEstimator::matchEnvironmentToLocations(std::vector<std::string> & vec){
	env.matchEnvironmentToLocations(vec);
	env2.matchEnvironmentToLocations(vec);
};

bool TDeltaSEstimator::readNext(){
	if(env.fillVectorOfNormalizedDensitiesForNextMCMCSample(densities1) && env2.fillVectorOfNormalizedDensitiesForNextMCMCSample(densities2)){
		if(densities1.size() != densities2.size())
			throw "Unequal number of locations!"; //should never happen
		return true;
	} else return false;
};

double TDeltaSEstimator::calculateCurrentDeltaS(){
	double delta = 0.0;
	for(size_t i=0; i<densities1.size(); ++i){
		delta += std::min(densities1[i], densities2[i]);
	}

	meanVar.add(delta);
	return delta;
};

double TDeltaSEstimator::calculateCurrentDeltaST(TDeltaTEstimator & deltaTEstimator, TMeanVar & thisMeanVar){
	double delta = 0.0;
	for(size_t i=0; i<densities1.size(); ++i){
		delta += deltaTEstimator.calculateCurrentDeltaTWeighted(densities1[i], densities2[i]);
	}

	thisMeanVar.add(delta);
	return delta;
};

void TDeltaSEstimator::printMeanVar(){
	meanVar.calcMeanVar();
	logfile->conclude("Mean deltaS = " + toString(meanVar.mean()));
	logfile->conclude("Variance deltaS = " + toString(meanVar.var()));
};

void TDeltaSEstimator::estimateDeltaSpace(TParameters & params){
	//open environments
	std::string outPrefix = openEnvironments(params);

	//read locations
	readLocations(params, outPrefix);

	//open output file
	std::string outFileName = outPrefix + "_deltaS.txt.gz";
	logfile->list("Will write delta posterior to '" + outFileName + "'.");

	TOutputFileZipped out(outFileName);
	out.writeHeader({"deltaS"});

	//limit lines
	int limitLines = params.getParameterIntWithDefault("limitLines", 100000000);
	logfile->startIndent("Comparing densities at up to " + toString(limitLines) + " posterior samples:");
	logfile->listOverFlush("Parsed 0 posterior samples ...");

	//parse through mcmc files
	while(meanVar.size() < limitLines && readNext()){
		//calculate and write deltaS
		double deltaS = calculateCurrentDeltaS();
		out << deltaS << std::endl;

		//report
		if(meanVar.size() % 100 == 0)
			logfile->listOverFlush("Parsed " + toString(meanVar.size()) + " posterior samples ...");
	}

	logfile->overList("Parsed " + toString(meanVar.size()) + " posterior samples ... reached end of file.");

	//conclude
	printMeanVar();
};

void TDeltaSEstimator::estimateDeltaSpaceTime(TParameters & params){
	//open environments
	std::string outPrefix = openEnvironments(params);

	//read locations
	readLocations(params, outPrefix);

	//open output file
	std::string outFileName = outPrefix + "_deltaST.txt.gz";
	logfile->list("Will write delta posterior to '" + outFileName + "'.");

	TOutputFileZipped out(outFileName);
	out.writeHeader({"deltaS", "deltaT", "deltaST"});

	//preprare mean var
	TMeanVar meanVarDeltaST;

	//open deltaT estimator
	TDeltaTEstimator deltaTEstimator(logfile);
	deltaTEstimator.initializeDeltaTEstimation(params);

	//limit lines
	int limitLines = params.getParameterIntWithDefault("limitLines", 100000000);
	logfile->startIndent("Comparing densities at up to " + toString(limitLines) + " posterior samples:");
	logfile->listOverFlush("Parsed 0 posterior samples ...");

	//parse through mcmc files
	while(meanVar.size() < limitLines && readNext()){
		//calculate deltaS
		double deltaS = calculateCurrentDeltaS();

		//calculate deltaT
		deltaTEstimator.readNext();
		double deltaT = deltaTEstimator.calculateCurrentDeltaT();

		//calculate deltaST
		double deltaST = calculateCurrentDeltaST(deltaTEstimator, meanVarDeltaST);

		//write
		out << deltaS << deltaT << deltaST << std::endl;

		//report
		if(meanVar.size() % 100 == 0)
			logfile->listOverFlush("Parsed " + toString(meanVar.size()) + " posterior samples ...");
	}

	logfile->overList("Parsed " + toString(meanVar.size()) + " posterior samples ... reached end of file.");

	//conclude
	printMeanVar();
	deltaTEstimator.printMeanVar();
	meanVarDeltaST.calcMeanVar();
	logfile->conclude("Mean deltaST = " + toString(meanVarDeltaST.mean()));
	logfile->conclude("Variance deltaST = " + toString(meanVarDeltaST.var()));
};
