/*
 * TMCMCFile.h
 *
 *  Created on: Mar 25, 2019
 *      Author: phaentu
 */

#ifndef TMCMCFILE_H_
#define TMCMCFILE_H_

#include "stringFunctions.h"
#include "gzstream.h"
#include "TEnvironment.h"

//---------------------------------------
// TMCMCFile
//---------------------------------------
class TMCMCFile{
protected:
	std::string filename;
	gz::igzstream file;
	bool isOpen;
	size_t numCols;
	int lineNum;
	int numSamples;
	bool _withAModel;

	void readHeader();
    void _updateEnvironment(TEnvironment & env, int offset);

public:
	std::vector<std::string> header;
	std::vector<double> values;

	TMCMCFile();
	TMCMCFile(std::string filename);
	~TMCMCFile();

	void open(std::string filename);
	bool withAModel(){return _withAModel;};
	bool eof(){ return file.eof() || !file.good(); };
	virtual bool readNext();
	int getNumSamplesRead(){ return numSamples; };
    virtual void setValues(TEnvironment & env);
    void checkForAModelInHeader();
};

//---------------------------------------
// TMCMCFile_lambda
//---------------------------------------

class TMCMCFile_lambda:public TMCMCFile{
public:
	TMCMCFile_lambda(){};
	TMCMCFile_lambda(std::string filename);
	void setValues(TEnvironment_lambda & env);
};

//---------------------------------------
// TMCMCFile_oneLine
//---------------------------------------

class TMCMCFile_oneLine:public TMCMCFile{
public:
    TMCMCFile_oneLine(){};
    TMCMCFile_oneLine(std::string filename);
    bool readNext() override;
};

//---------------------------------------
// TMCMCFile_lambda_oneLine
//---------------------------------------

class TMCMCFile_lambda_oneLine: public TMCMCFile_lambda{
public:
    TMCMCFile_lambda_oneLine(){};
    TMCMCFile_lambda_oneLine(std::string filename);
    bool readNext() override;
};

#endif /* TMCMCFILE_H_ */
