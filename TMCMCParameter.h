/*
 * TMCMCParameter.h
 *
 *  Created on: Dec 19, 2018
 *      Author: phaentu
 */

#ifndef TMCMCPARAMETER_H_
#define TMCMCPARAMETER_H_

#include "TRandomGenerator.h"

//-------------------------------------------
// TPrior
//-------------------------------------------
class TPrior{
private:

public:
	TPrior(){};
	virtual ~TPrior(){};
	virtual double getLogPriorDensity(double x){ return 0.0; };
	virtual double getRandomValue(TRandomGenerator* randomGenerator){ throw "Can not draw random number from U(-inf, inf) prior!"; };
    virtual void initialize(double val1){throw std::runtime_error("Function 'virtual void initialize(double val1)' is not implemented for base class TPrior!");};
    virtual void initialize(double val1, double val2){throw std::runtime_error("Function 'virtual void initialize(double val1, double val2)' is not implemented for base class TPrior!");};
};

class TPriorUniform:public TPrior{
private:
	double min, max;
	double logDensity;

public:
	TPriorUniform();
	TPriorUniform(double Min, double Max);
	void initialize(double Min, double Max);

	double getLogPriorDensity(double x);
	double getRandomValue(TRandomGenerator* randomGenerator);
};

class TPriorNormal:public TPrior{
private:
	double mean, sd;
	double logOneOverSqrtTwoPiSigma, twoSdSquared; double sqrtTwoPi;

public:
	TPriorNormal();
	TPriorNormal(double Mean, double Sd);
	void initialize(double Mean, double Sd);
	double getLogPriorDensity(double x);
	double getRandomValue(TRandomGenerator* randomGenerator);
};

class TPriorExponential:public TPrior{
private:
	double lambda;
	double logLambda;

public:
	TPriorExponential();
	TPriorExponential(double Lambda);
	void initialize(double Lambda);
	double getLogPriorDensity(double x);
	double getRandomValue(TRandomGenerator* randomGenerator);
};


//-------------------------------------------
// TMCMCParameter
//-------------------------------------------
class TMCMCParameter{
protected:
	double propSD;
	int updates;
	int acceptedUpdates;
	TPrior* prior;
	bool hasPrior;

public:
	double value;
	double value_old;

	TMCMCParameter();
	TMCMCParameter(double val);
	virtual ~TMCMCParameter(){};

	void addPrior(TPrior* Prior){ prior = Prior; hasPrior = true; };
	void set(double val){ value_old = value; value = val; };
	void scale(double Scale){ value_old = value; value *= Scale; };
	void setPropSD(double SD){ propSD = SD; };
    double getPropSD(){ return propSD; };
    int getNumUpdates(){return updates;};
    virtual double update(TRandomGenerator* & randomGenerator); //return log(proposal ratio)
	virtual void reject();
	void reset(){ value = value_old; };
	virtual void adjustProposal();
	virtual double getAcceptanceRate();
};

class TMCMCParameterTrackExp : public TMCMCParameter {
public:
    // NOTE: 'value' is in log. 'expValue' is not in log.
    double expValue;
    double expValue_old;

    TMCMCParameterTrackExp();
    TMCMCParameterTrackExp(double val);
    ~TMCMCParameterTrackExp(){};

    void set(double val);
    void scale(double Scale);
    virtual double update(TRandomGenerator* & randomGenerator); //return log(proposal ratio)
    virtual void reject();
    void reset();
};

class TMCMCParameterPositive:public TMCMCParameter{
public:
	TMCMCParameterPositive():TMCMCParameter(){};
	TMCMCParameterPositive(double val):TMCMCParameter(val){};
	~TMCMCParameterPositive(){};

	double update(TRandomGenerator* & randomGenerator);
};

class TMCMCParameterZeroOne:public TMCMCParameter{
public:
	TMCMCParameterZeroOne():TMCMCParameter(){};
	TMCMCParameterZeroOne(double val):TMCMCParameter(val){};
	~TMCMCParameterZeroOne(){};

	double update(TRandomGenerator* & randomGenerator);
	void adjustProposal();
};

class TMCMCParameterMixture:public TMCMCParameter{
private:
	int _counter_1_Model;
    int _counter_1_Model_Positive;
    int _counter_1_Model_Negative;
    TPrior* _model_0; //distribution of null model
	TPrior* _model_1; // distribution of non-null model
	TMCMCParameter* _pi; //hierarchical probability that A != 0

    double _propSD_1;
    int _updates_1;
    int _acceptedUpdates_1;

    void _init();
    void _updateModel(TRandomGenerator* & randomGenerator);

public:
	uint8_t model;

	TMCMCParameterMixture();
	TMCMCParameterMixture(double val);
	~TMCMCParameterMixture(){};

	void set(double val, uint8_t whichModel);
    void addPrior(TPrior* Prior0, TPrior* prior1);
    void addParameters(TMCMCParameter* Pi);
    double calculateInclusionProbability();
    double calculateInclusionProbabilityPositive();
    double calculateInclusionProbabilityNegative();

    // proposal kernel
    void setPropSD_1(double SD){ _propSD_1 = SD; };
    double getPropSD_1(){ return _propSD_1; };
    int getNumUpdates_0(){return updates;};
    int getNumAcceptedUpdates_0(){return acceptedUpdates;};
    int getNumUpdates_1(){return _updates_1;};
    int getNumAcceptedUpdates_1(){return _acceptedUpdates_1;};

    // updating
    double update(TRandomGenerator* & randomGenerator); //return log(proposal ratio)
    virtual void reject();
    void adjustProposal(double newSd0, double newSd1);
    double getAcceptanceRate_1() const;
};



#endif /* TMCMCPARAMETER_H_ */
