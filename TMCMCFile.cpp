/*
 * TMCMCFile.cpp
 *
 *  Created on: Mar 25, 2019
 *      Author: phaentu
 */

#include "TMCMCFile.h"

//---------------------------------------
// TMCMCFile
//---------------------------------------
TMCMCFile::TMCMCFile(){
	isOpen = false;
	lineNum = 0;
	numSamples = 0;
	numCols = 0;
	_withAModel = false;
};

TMCMCFile::TMCMCFile(std::string Filename){
	open(Filename);
};

TMCMCFile::~TMCMCFile(){
	if(isOpen)
		file.close();
};

void TMCMCFile::open(std::string Filename){
	filename = Filename;
	if(filename.empty()) throw "Please provide a valid filename!";
	file.open(filename.c_str());
	if(!file)
		throw "Failed to open file '" + filename + "' for reading!";

	isOpen = true;
	lineNum = 0;
	numSamples = 0;

	//read header
	readHeader();
};

void TMCMCFile::readHeader(){
	std::string line;
	getline(file, line);
	fillVectorFromStringWhiteSpaceSkipEmpty(line, header);
	numCols = header.size();
	++lineNum;
};

bool TMCMCFile::readNext(){
	if(!isOpen)
		throw "MCMC file was never opend!";
	if(file.eof() || !file.good())
		return false;

	std::string line;
	getline(file, line);
	fillVectorFromStringWhiteSpaceSkipEmpty(line, values);
	++lineNum;

	//skip empty lines
	if(values.size() == 0) return readNext();

	//check if there are three columns: env, mean, sd
	if(values.size() != numCols)
		throw("Wrong number of columns in file '" + filename + "' on line " + toString(lineNum) + "! Expected " + toString(numCols) + " but found " + toString(values.size()) + ".");

	++numSamples;
	return true;
};

void TMCMCFile::_updateEnvironment(TEnvironment & env, int offset){
    if (_withAModel){
        // set both A and model of A        
        std::vector<double> envVals;
        std::vector<uint8_t> envModels;
        int numEnvVar = static_cast<int>(((double) values.size() - (double) offset) / 2.);

        std::cout << "offset = " << offset << std::endl;
        std::cout << "numEnvVar = " << numEnvVar << std::endl;
        std::cout << "values.size() = " << values.size() << std::endl;

        envVals.assign(values.begin() + offset, values.begin() + offset + numEnvVar);
        envModels.assign(values.begin() + offset + numEnvVar, values.end());
        env.set_A(envVals, envModels);
    } else {
        // set A, initialize model of A to 1 for all
        std::vector<double> envVals;
        int numEnvVar = values.size() - offset;
        envVals.assign(values.begin() + offset, values.end());
        std::vector<uint8_t> envModels(numEnvVar, 1);
        env.set_A(envVals, envModels);
    }
}

void TMCMCFile::checkForAModelInHeader(){
    for (auto & it : header){
        if (stringContains(it, "A_model_")) {
            _withAModel = true;
            break;
        }
    }
}

void TMCMCFile::setValues(TEnvironment & env){
    int offset = 5; // the first 5 variables in MCMC file are LL, posterior, pi, rho and sigma0
    env.set_pi(values[2]);
    env.set_rho(values[3]);
    env.set_sigma_0(values[4]);

    //update environment
    _updateEnvironment(env, offset);
};

//---------------------------------------
// TMCMCFile_lambda
//---------------------------------------
TMCMCFile_lambda::TMCMCFile_lambda(std::string filename):TMCMCFile(filename){

};

void TMCMCFile_lambda::setValues(TEnvironment_lambda & env){
    int offset = 6; // the first 6 variables in MCMC file are LL, posterior, a0, pi, rho and sigma0
    env.set_a0(values[2]);
    env.set_pi(values[3]);
    env.set_rho(values[4]);
    env.set_sigma_0(values[5]);

    //update environment
    _updateEnvironment(env, offset);
};

//---------------------------------------
// TMCMCFile_oneLine
//---------------------------------------

TMCMCFile_oneLine::TMCMCFile_oneLine(std::string filename) : TMCMCFile(filename){}

bool TMCMCFile_oneLine::readNext(){
    // contains header + one line with values
    // used for files that contain simulated values (truth); these have also the format of MCMCFiles
    if (lineNum == 1){
        return TMCMCFile::readNext();
    } else return true; // don't read, just return true
};

//---------------------------------------
// TMCMCFile_lambda_oneLine
//---------------------------------------

TMCMCFile_lambda_oneLine::TMCMCFile_lambda_oneLine(std::string filename) : TMCMCFile_lambda(filename){}

bool TMCMCFile_lambda_oneLine::readNext(){
    // contains header + one line with values
    // used for files that contain simulated values (truth); these have also the format of MCMCFiles
    if (lineNum == 1){
        return TMCMCFile_lambda::readNext();
    } else return true; // don't read, just return true
};
