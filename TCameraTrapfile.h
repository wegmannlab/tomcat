/*
 * TCameraTrapfile.h
 *
 *  Created on: Dec 18, 2018
 *      Author: phaentu
 */

#ifndef TCAMERATRAPFILE_H_
#define TCAMERATRAPFILE_H_

#include "TLog.h"
#include "gzstream.h"
#include "stringFunctions.h"

class TCameraTrapfile{
private:
	std::string filename;
	std::istream* in;
	size_t numCols;
	int lineNum;
	bool fileOpen;

	//tmp variables
	std::vector<std::string> vec;
	std::string line;

public:
	std::vector<std::string> envNames;
	std::string trapName;
	time_t trapStart, trapEnd;
	std::vector<double> envVariables;

	TCameraTrapfile();
	TCameraTrapfile(const std::string & filename, std::string, TLog* logfile);
	~TCameraTrapfile(){ close(); };

	void close();

	void open(const std::string & filename, std::string tag, TLog* logfile);
	bool readNext();
	std::string getLineString();

};



#endif /* TCAMERATRAPFILE_H_ */
