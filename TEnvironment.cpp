/*
 * TEnvironment.cpp
 *
 *  Created on: Dec 16, 2018
 *      Author: phaentu
 */

#include "TEnvironment.h"

//-------------------------------------------
// TEnvironment
//-------------------------------------------
TEnvironment::TEnvironment(){
	_numEnv = 0;
	prior_pi = nullptr;
    prior_sigma_0 = nullptr;
    prior_rho = nullptr;
	_model_0 = nullptr;
	_model_1 = nullptr;
	logPriorProposalRatio = 0.0;

	//parameters
	_A = NULL;
	changedIndex_A = 0;
	hasChanged_A = false;
	updatedParameter = NULL;
};

TEnvironment::TEnvironment(int NumEnv){
	_numEnv = NumEnv;
	_initialize(NumEnv);
};

TEnvironment::TEnvironment(std::vector<std::string> & Names){
	_numEnv = 0;
	initialize(Names);
};

TEnvironment::~TEnvironment(){
	clear();
};

void TEnvironment::initialize(std::vector<std::string> & Names){
	_initialize(Names.size());
	envNames = Names;
};

void TEnvironment::_initialize(int NumEnv){
	clear();

	_numEnv = NumEnv;
	_pi.set(0.);
	_rho.set(0.);
	_sigma_0.set(0.);
	_A = new TMCMCParameterMixture[_numEnv];

	set_A_zero();
    setParameters_A();

	//default names
	for(size_t e=0; e<_numEnv; ++e)
		envNames.push_back("Env_" + toString(e+1));

	//restart parameter updating
	hasChanged_A = false;
};

void TEnvironment::clear(){
	if(_numEnv > 0){
		delete[] _A;
		_numEnv = 0;
	}
};

void TEnvironment::set_sigma_0(double sigma0){
    _sigma_0.set(sigma0);
    if (_model_0 != nullptr)
        _model_0->initialize(0, _sigma_0.value);
    if (_model_1 != nullptr)
        _model_1->initialize(0, _calcSigma1FromRhoAndSigma0());
};

void TEnvironment::set_rho(double rho) {
     _rho.set(rho);
    if (_model_1 != nullptr)
        _model_1->initialize(0, _calcSigma1FromRhoAndSigma0());
}

void TEnvironment::set_A_zero(){
	for(size_t e=0; e<_numEnv; ++e)
		_A[e].set(0.0, 0);
};

void TEnvironment::set_A(std::vector<double> & A, std::vector<uint8_t> & A_model){
	if(A.size() != _numEnv)
		throw "Can not set A: size of provided vector (" + toString(A.size()) + ") is unequal number of environmental variables (" + toString(_numEnv) + ")!";
    if(A_model.size() != _numEnv)
        throw "Can not set A_model: size of provided vector unequal number of environmental variables!";

	for(size_t e=0; e<_numEnv; ++e)
		_A[e].set(A[e], A_model[e]);
};

void TEnvironment::setPrior_A(TPrior* Prior_0, TPrior* Prior_1){
    _model_0 = Prior_0;
    _model_1 = Prior_1;

	for(size_t e=0; e<_numEnv; ++e)
		_A[e].addPrior(Prior_0, Prior_1);
};

void TEnvironment::setParameters_A(){
	for(size_t e=0; e<_numEnv; ++e)
		_A[e].addParameters(&_pi);
};

void TEnvironment::initializeSigma0() {
    // calculates MLE variance of A and initializes sigma0 accordingly
    TMeanVar var_0_model;
    for(size_t e=0; e<_numEnv; ++e){
        if (_A[e].model == 0) var_0_model.add(_A[e].value);
    }

    // set sigma0
    double sigma0 = var_0_model.sd();
    if (sigma0 <= 0.) sigma0 = 0.001;
    set_sigma_0(sigma0);
}

void TEnvironment::initializeRho() {
    // calculates MLE variance of A and initializes rho accordingly
    TMeanVar var_1_model;
    for(size_t e=0; e<_numEnv; ++e){
        if (_A[e].model == 1) var_1_model.add(_A[e].value);
    }

    // set rho
    double sigma1 = var_1_model.sd();
    if (sigma1 <= 0.) sigma1 = 0.01;
    double rho = _calcRhoFromSigma1(sigma1);
    if (rho <= 0) rho = 0.01;
    set_rho(rho);

    _rho.setPropSD(100);
}

void TEnvironment::simulate(int NumEnv_nonZero, int NumEnv_zero, double sigma0, double rho, TRandomGenerator* randomGenerator){
	_initialize(NumEnv_nonZero + NumEnv_zero);

	set_pi((double) NumEnv_nonZero / (double)(NumEnv_nonZero + NumEnv_zero));
    _sigma_0.set(sigma0);
    _rho.set(rho);
    double sigma1 = _calcSigma1FromRhoAndSigma0();

	size_t e = 0;
    for(; e<NumEnv_nonZero; ++e) // first 'NumEnv_nonZero' A's come from 1-model
        _A[e].set(randomGenerator->getNormalRandom(0., sigma1), 1);
	for(; e < NumEnv_nonZero + NumEnv_zero; ++e) // all other A's come from 0-model
        _A[e].set(randomGenerator->getNormalRandom(0., sigma0), 0);
};

void TEnvironment::simulate(int NumEnv_nonZero, int NumEnv_zero, const std::vector<double> & A,  TRandomGenerator* randomGenerator){
	_initialize(NumEnv_nonZero + NumEnv_zero);

	set_pi((double) NumEnv_nonZero / (double)(NumEnv_nonZero + NumEnv_zero));

    size_t e = 0;
    for(; e<NumEnv_nonZero; ++e) // first 'NumEnv_nonZero' A's come from 1-model
        _A[e].set(A[e], 1);
    for(; e < NumEnv_nonZero + NumEnv_zero; ++e) // all other A's come from 0-model
        _A[e].set(A[e], 0);
};

void TEnvironment::simulateEnvironmentalVariables(std::vector<double> & vec, TRandomGenerator* randomGenerator){
	vec.clear();
	for(size_t e=0; e<_numEnv; ++e){
		vec.push_back(randomGenerator->getNormalRandom(0.0, 1.0));
	}
};

int TEnvironment::getIndexFromName(const std::string & name){
	for(size_t i=0; i<envNames.size(); ++i){
		if(envNames[i] == name)
			return i;
	}
	return -1;
};

void TEnvironment::addNamesToVector(std::vector<std::string> & vec){
	for(size_t i=0; i<envNames.size(); ++i)
		vec.emplace_back(envNames[i]);
};

std::string TEnvironment::getString_A(){
	std::string ret = toString(_A[0].value);
	for(size_t e=1; e<_numEnv; ++e)
		ret += ", " + toString(_A[e].value);
	return ret;
};

double TEnvironment::getLinearCombination(std::vector<double> & environmentalVariables, double & linearComb){
	linearComb = 0.0;
	for(size_t e=0; e<_numEnv; ++e)
		linearComb += environmentalVariables[e] * _A[e].value;

	return linearComb;
};

double TEnvironment::updateLinearCombination(std::vector<double> & environmentalVariables, double & linearComb){
	if(hasChanged_A){
		linearComb += environmentalVariables[changedIndex_A] * (_A[changedIndex_A].value - _A[changedIndex_A].value_old);
	}

	return linearComb;
};

//-----------------------------------------------------------------
// MCMC updates
//-----------------------------------------------------------------
void TEnvironment::update_pi(TRandomGenerator* randomGenerator){
	//update
	_pi.update(randomGenerator);

	//calc hastings ratio
	double log_h = prior_pi->getLogPriorDensity(_pi.value) - prior_pi->getLogPriorDensity(_pi.value_old);

    log_h += _calcP_AModel_given_pi();

	//accept or reject?
	if(log(randomGenerator->getRand()) > log_h){
		_pi.reject();
	}
};

double TEnvironment::_calcP_AModel_given_pi(){
    int numOne = 0;
    for(size_t e=0; e<_numEnv; ++e){
        if(_A[e].model == 1)
            ++numOne;
    }

    double LL = (_numEnv - numOne) * log((1. - _pi.value) / (1. - _pi.value_old)) + numOne * log(_pi.value / _pi.value_old);

    return LL;
}

double TEnvironment::_calcP_A_given_sigma0_and_rho(){
    double LL = 0.;
    for(size_t e=0; e<_numEnv; ++e) {
        if (_A[e].model == 0)
            LL += _model_0->getLogPriorDensity(_A[e].value);
        else
            LL += _model_1->getLogPriorDensity(_A[e].value);
    }
    return LL;
}

void TEnvironment::update_sigma_0(TRandomGenerator* randomGenerator){
    // NOTE: when updating sigma_0, sigma_1 automatically changes -> need to consider
    // density of both mixture distributions in hastings ratio!

    // first calculate log likelihood before updating (-> old LL, could be stored)
    double LL_old = _calcP_A_given_sigma0_and_rho();

    // update sigma_0 and models
    _sigma_0.update(randomGenerator);
    _model_0->initialize(0, _sigma_0.value);
    _model_1->initialize(0, _calcSigma1FromRhoAndSigma0());

    // calculate new log likelihood
    double LL_new = _calcP_A_given_sigma0_and_rho();

    //calc hastings ratio
    double priorRatio = prior_sigma_0->getLogPriorDensity(_sigma_0.value) - prior_sigma_0->getLogPriorDensity(_sigma_0.value_old);
    double log_h = LL_new - LL_old + priorRatio;

    //accept or reject?
    if(log(randomGenerator->getRand()) > log_h){
        // reject
        _sigma_0.reject();
        _model_0->initialize(0, _sigma_0.value);
        _model_1->initialize(0, _calcSigma1FromRhoAndSigma0());
    }
};

double TEnvironment::_calcP_A_given_sigma_1(){
    double LL = 0.;
    for(size_t e=0; e<_numEnv; ++e) {
        if (_A[e].model == 1)
            LL += _model_1->getLogPriorDensity(_A[e].value);
    }
    return LL;
}

void TEnvironment::update_rho(TRandomGenerator* randomGenerator){
    // first calculate "old" log likelihood (-> could be stored)
    double LL_old = _calcP_A_given_sigma_1();

    // update rho and corresponding model
    _rho.update(randomGenerator);
    _model_1->initialize(0, _calcSigma1FromRhoAndSigma0());

    // calculate new log likelihood
    double LL_new = _calcP_A_given_sigma_1();

    //calc hastings ratio
    double priorRatio = prior_rho->getLogPriorDensity(_rho.value) - prior_rho->getLogPriorDensity(_rho.value_old);
    double log_h = LL_new - LL_old + priorRatio;

    //accept or reject?
    if(log(randomGenerator->getRand()) > log_h){
        // reject
        _rho.reject();
        _model_1->initialize(0, _calcSigma1FromRhoAndSigma0());
    }
};

bool TEnvironment::updateNext_A(TRandomGenerator* randomGenerator){
	//do we restart? Else advance
	if(!hasChanged_A){
		hasChanged_A = true;
		changedIndex_A = 0;
	} else {
		changedIndex_A++;
		if(changedIndex_A == _numEnv){
			hasChanged_A = false;
			return false;
		}
	}

	//set pointer to parameter to update and update
	updatedParameter = &_A[changedIndex_A];
	logPriorProposalRatio = updatedParameter->update(randomGenerator);
	return true;
};

void TEnvironment::rejectLastUpdate_A(){
	updatedParameter->reject();
};

double TEnvironment::_calcNewPropKernel(double oldPropKernel, int numUpdates, double meanAccRate){
    double newPropKernel = oldPropKernel;
    if (numUpdates > 1){
        double scale = 3.0 * meanAccRate;
        if(scale < 0.1) scale = 0.1;

        //update
        newPropKernel *= scale;
    }
    return newPropKernel;
}

int TEnvironment::_fillMeanAcceptanceRate_A0(double & meanAccRate){
    // now the same for the 1-model
    int numAcceptedUpdates = 0;
    int numUpdates = 0;

    for(size_t e=0; e<_numEnv; ++e){
        numAcceptedUpdates += _A[e].getNumAcceptedUpdates_0();
        numUpdates += _A[e].getNumUpdates_0();
        if (_A[0].getPropSD() != _A[e].getPropSD())
            throw std::runtime_error("In function 'void TEnvironment::_adjustProposalRanges_A()': Proposal kernels should be equal for all A!");
    }
    meanAccRate = (double) numAcceptedUpdates / (double) numUpdates;

    return numUpdates;
}

double TEnvironment::_calcNewPropKernel_A0(){
    double meanAccRate = 0;
    int numUpdates = _fillMeanAcceptanceRate_A0(meanAccRate);

    // calculate new proposal kernel
    return _calcNewPropKernel(_A[0].getPropSD(), numUpdates, meanAccRate);
}


int TEnvironment::_fillMeanAcceptanceRate_A1(double & meanAccRate){
    // now the same for the 1-model
    int numAcceptedUpdates = 0;
    int numUpdates = 0;

    for(size_t e=0; e<_numEnv; ++e){
        numAcceptedUpdates += _A[e].getNumAcceptedUpdates_1();
        numUpdates += _A[e].getNumUpdates_1();
        if (_A[0].getPropSD_1() != _A[e].getPropSD_1())
            throw std::runtime_error("In function 'void TEnvironment::_adjustProposalRanges_A()': Proposal kernels should be equal for all A!");
    }
    meanAccRate = (double) numAcceptedUpdates / (double) numUpdates;

    return numUpdates;
}

double TEnvironment::_calcNewPropKernel_A1(){
    double meanAccRate = 0;
    int numUpdates = _fillMeanAcceptanceRate_A1(meanAccRate);

    // calculate new proposal kernel
    return _calcNewPropKernel(_A[0].getPropSD_1(), numUpdates, meanAccRate);
}

void TEnvironment::_adjustProposalRanges_A(){
    double newPropKernel_0 = _calcNewPropKernel_A0();
    double newPropKernel_1 = _calcNewPropKernel_A1();

    // now set!
    for(size_t e=0; e<_numEnv; ++e){
        _A[e].adjustProposal(newPropKernel_0, newPropKernel_1);
    }
}

void TEnvironment::adjustProposalRanges(){
    // adjust proposal kernel of A: special, because 0-model and 1-model share a proposal kernel each
    _adjustProposalRanges_A();

	_pi.adjustProposal();
	_rho.adjustProposal();
	_sigma_0.adjustProposal();
};

double TEnvironment::calculatePrior(){
    // sum log P(A | sigma0, rho)
    double logP_A_given_sigma0_and_rho = _calcP_A_given_sigma0_and_rho();
    // sum log P(A_model | pi)
    double logP_AModel_given_pi = _calcP_AModel_given_pi();
    // log P(sigma0 | rate_sigma0)
    double logP_sigma0 = 0.;
    if (prior_sigma_0 != nullptr)
        logP_sigma0 = prior_sigma_0->getLogPriorDensity(_sigma_0.value);
    // log P(rho | rate_rho)
    double logP_rho = 0.;
    if (prior_rho != nullptr)
        logP_rho = prior_rho->getLogPriorDensity(_rho.value);
    // log P(pi | rate_pi)
    double logP_pi = 0.;
    if (prior_pi != nullptr)
        logP_pi = prior_pi->getLogPriorDensity(_pi.value);

    return logP_A_given_sigma0_and_rho + logP_AModel_given_pi + logP_sigma0 + logP_rho + logP_pi;
}

void TEnvironment::writeMCMCHeader(gz::ogzstream & out, bool printFullLambdaModel){
    out << "LL\tposterior\tpi\trho\tsigma0";
    for(size_t e=0; e<_numEnv; ++e)
		out << "\tA_" << envNames[e];
    if (printFullLambdaModel) {
        for (size_t e = 0; e < _numEnv; ++e)
            out << "\tA_model_" << envNames[e];
    }
	out << "\n";
};

void TEnvironment::writeMCMCValues(gz::ogzstream & out, const double LL, const double posterior, bool printFullLambdaModel){
    out << LL << "\t" << posterior << "\t" << _pi.value << "\t" << _rho.value << "\t" << _sigma_0.value;
	for(size_t e=0; e<_numEnv; ++e)
		out << "\t" << _A[e].value;
	if (printFullLambdaModel) {
        for (size_t e = 0; e < _numEnv; ++e)
            out << "\t" << (int) _A[e].model;
    }
	out << "\n";
};

void TEnvironment::writeInclusionProbs(std::ofstream & out){
    // write header
    out << "inclusionProbability";
    for(size_t e=0; e<_numEnv; ++e)
        out << "\tA_" << envNames[e];
    out << "\n";

    // write inclusion probabilities total
    out << "total";
    for(size_t e=0; e<_numEnv; ++e)
        out << "\t" << _A[e].calculateInclusionProbability();
    out << "\n";

    // write inclusion probabilities positive
    out << "positive";
    for(size_t e=0; e<_numEnv; ++e)
        out << "\t" << _A[e].calculateInclusionProbabilityPositive();
    out << "\n";

    // write inclusion probabilities negative
    out << "negative";
    for(size_t e=0; e<_numEnv; ++e)
        out << "\t" << _A[e].calculateInclusionProbabilityNegative();
    out << "\n";
};

void TEnvironment::printAcceptanceRates(TLog* logfile, bool estimatePi, bool estimateRho, bool estimateSigma0){
    double meanAccRate_A0 = 0.;
    _fillMeanAcceptanceRate_A0(meanAccRate_A0);
    double meanAccRate_A1 = 0.;
    _fillMeanAcceptanceRate_A1(meanAccRate_A1);
	std::string s = toString(meanAccRate_A0) + ", " + toString(meanAccRate_A1);
	logfile->list("A = " + s);

    if(estimatePi)
		logfile->list("pi = " + toString(_pi.getAcceptanceRate()));
	if(estimateRho)
        logfile->list("rho = " + toString(_rho.getAcceptanceRate()));
    if(estimateSigma0)
        logfile->list("sigma_0 = " + toString(_sigma_0.getAcceptanceRate()));
};

//-----------------------------------------------------------------
// TEnvironment_p
//-----------------------------------------------------------------
double TEnvironment_p::getParamValue(std::vector<double> & environmentalVariables, double & linearComb){
	return 1.0 / (1.0 + exp(getLinearCombination(environmentalVariables, linearComb)));
};

double TEnvironment_p::updateParamValue(std::vector<double> & environmentalVariables, double & linearComb){
	return 1.0 / (1.0 + exp(getLinearCombination(environmentalVariables, linearComb)));
};

//-----------------------------------------------------------------
// TEnvironment_lambda
//-----------------------------------------------------------------
double TEnvironment_lambda::getParamValue(std::vector<double> & environmentalVariables, double & linearComb){
	//turn into rate per second: remove log(24*60*60) = 11.36674
	return exp(getLinearCombination(environmentalVariables, linearComb) + _a0.value - 11.36674);
};

double TEnvironment_lambda::updateParamValue(std::vector<double> & environmentalVariables, double & linearComb){
	//turn into rate per second: remove log(24*60*60) = 11.36674
	return exp(getLinearCombination(environmentalVariables, linearComb) + _a0.value - 11.36674);
};

void TEnvironment_lambda::_initialize(int NumEnv){
	TEnvironment::_initialize(NumEnv);
	_a0.set(0.0);
};

void TEnvironment_lambda::simulate(int NumEnv_nonZero, int NumEnv_zero, double sigma0, double rho, double a0, TRandomGenerator* randomGenerator){
	TEnvironment::simulate(NumEnv_nonZero, NumEnv_zero, sigma0, rho, randomGenerator);
	set_a0(a0);
};

void TEnvironment_lambda::simulate(int NumEnv_nonZero, int NumEnv_zero, double a0, const std::vector<double> & A, TRandomGenerator* randomGenerator){
	TEnvironment::simulate(NumEnv_nonZero, NumEnv_zero, A, randomGenerator);
	set_a0(a0);
};

void TEnvironment_lambda::adjustProposalRanges(){
	TEnvironment::adjustProposalRanges();
	_a0.adjustProposal();
};

void TEnvironment_lambda::update_a0(TRandomGenerator* randomGenerator){
	logPriorProposalRatio = _a0.update(randomGenerator);
};

void TEnvironment_lambda::reject_a0(){
	_a0.reject();
};

double TEnvironment_lambda::calculatePrior(){
    double posterior = TEnvironment::calculatePrior();
    posterior += prior_a0->getLogPriorDensity(_a0.value);

    return posterior;
}

void TEnvironment_lambda::writeMCMCHeader(gz::ogzstream & out, bool printFullLambdaModel){
	out << "LL\tposterior\ta0\tpi\trho\tsigma0";
	for(size_t e=0; e<_numEnv; ++e)
		out << "\tA_" << envNames[e];
	if (printFullLambdaModel) {
        for (size_t e = 0; e < _numEnv; ++e)
            out << "\tA_model_" << envNames[e];
    }
	out << "\n";
};

void TEnvironment_lambda::writeMCMCValues(gz::ogzstream & out, const double LL, const double posterior, bool printFullLambdaModel){
	out << LL << "\t" << posterior << "\t" << _a0.value << "\t" << _pi.value << "\t" << _rho.value << "\t" << _sigma_0.value;
	for(size_t e=0; e<_numEnv; ++e)
		out << "\t" << _A[e].value;
	if (printFullLambdaModel) {
        for (size_t e = 0; e < _numEnv; ++e)
            out << "\t" << (int) _A[e].model;
    }
	out << "\n";
};

void TEnvironment_lambda::printAcceptanceRates(TLog* logfile, bool estimatePi,  bool estimateRho,  bool estimateSigma0){
	logfile->list("a0 = " + toString(_a0.getAcceptanceRate()));
	TEnvironment::printAcceptanceRates(logfile, estimatePi, estimateRho, estimateSigma0);
};


